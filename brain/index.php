<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  $APPLICATION->SetPageProperty("title", "Bino");
  $APPLICATION->SetTitle("Bino");
  $APPLICATION->SetPageProperty("CLASS", "main");
?> 
  
<div class="general">
        <div class="general__overlay" style="background-image: url(assets/images/video.png)"></div><video loop muted playsinline autoplay poster="assets/images/video.png" class="general__video">
<source src="assets/video/Downhill.mp4" type="video/mp4">
<source src="assets/video/Downhill.webm" type="video/webm">
</video>
        <div class="container">
          <div class="general__inner js-animate animated" data-anim="fadeInUp">
            <div class="general__content"><span class="border-left"> </span><span class="border-top"></span><span class="border-bottom"></span>
              <div class="general__text">
                <p>Инженерная компания</p>
                <p>совершенства систем</p>
                <p>проектировки</p>
              </div>
            </div>
            <div class="general__description">
              <p>Опыт воплощенный в качестве</p>
              <p>работы любой сложности</p>
            </div>
          </div>
        </div>
      </div>
      <div class="promo pos-r">
        <div class="container">
          <div class="promo__inner js-animate animated" data-anim="fadeInUp">
            <div class="promo__img">
              <div class="promo__puls js-puls"></div>
              <div class="promo__puls js-puls"></div>
              <div class="promo__puls js-puls"></div>
              <div class="promo__puls js-puls"></div><img class="promo__pic" src="assets/images/brain.png" alt=""/>
            </div>
            <div class="promo__content">
              <div class="promo__title title title_block"><span>Краткого текста</span><span class="title__text_bg">заголовок</span></div>
              <div class="promo__description">
                <p>Здесь может быть какая-нибудь цитата. </p>
                <p>Все выполняемые работы сопровождаются оформлением технической документации. По завершении cтроительно монтажных работ комплект сполнительно-технической документации (ИТД) утверждается в ГРО, Ростехнадзоре и передается Заказчику для дальнейшего оформления в собственность и ввода объекта в эксплуатацию.</p>
              </div>
              <div class="promo__button button-border"><a class="button" href="#" target="_blank">Подробнее</a><span class="button-border__top-left"></span><span class="button-border__top-right"></span><span class="button-border__left"></span><span class="button-border__right"></span><span class="button-border__bottom-left"></span><span class="button-border__bottom-right"></span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="service-section pos-r">
        <div class="grid">
          <div class="container">
            <div class="grid__inner">
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="service-section__title title js-animate animated" data-anim="fadeInUp"><span>Наши</span><span class="title__text_bg">услуги</span></div>
          <div class="service pos-r grid-front js-animate animated" data-anim="fadeInUp"><a class="service__item service__item_color1" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img class="service__pic" src="assets/images/stroit.svg" alt=""/></span><span class="service__description">Строительно-монтажные работы</span></span></a><a class="service__item service__item_color2" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img class="service__pic" src="assets/images/proekt.svg" alt=""/></span><span class="service__description">Проектирование инженерных систем </span></span></a><a class="service__item service__item_color3" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img class="service__pic" src="assets/images/priboru.svg" alt=""/></span><span class="service__description">Приборы учета тепловой энергии </span></span></a><a class="service__item service__item_color4" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img class="service__pic" src="assets/images/to_pusk.svg" alt=""/></span><span class="service__description">Строительно-монтажные работы </span></span></a><a class="service__item service__item_color5" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img class="service__pic" src="assets/images/proizvodstv.svg" alt=""/></span><span class="service__description">Техническое обслуживание Пуско-наладочные работы</span></span></a><a class="service__item service__item_color6" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner service__item-inner_bottom"><span class="service__wrapper"><img class="service__pic" src="assets/images/dispet4.svg" alt=""/></span><span class="service__description">Диспетчеризация</span></span></a></div>
        </div>
      </div>
      <div class="popular pos-r">
        <div class="grid grid_invert">
          <div class="container">
            <div class="grid__inner">
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
            </div>
          </div>
        </div>
        <div class="container pos-r grid-front">
          <div class="popular__title title js-animate animated" data-anim="fadeInUp"><span class="title__text_bg">Популярные</span><span class="title__text_color">услуги</span></div>
          <div class="popular__inner js-wrapper js-animate animated" data-anim="fadeInUp">
            <div class="popular__column">
              <div class="popular__item js-title"><span class="popular__service">Строительно-монтажные работы</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer1.png)"><img class="popular__content-pic" src="assets/images/Layer1.png" alt=""/></div>
              <div class="popular__item js-title"><span class="popular__service">Проектирование инженерных систем</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer2.jpg)"><img class="popular__content-pic" src="assets/images/Layer2.jpg" alt=""/></div>
              <div class="popular__item js-title"><span class="popular__service">Приборы учета тепловой энергии</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer3.png)"><img class="popular__content-pic" src="assets/images/Layer3.png" alt=""/></div>
              <div class="popular__item js-title"><span class="popular__service">Стройконтроль за работами в области инженерных сетей</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer2.jpg)"><img class="popular__content-pic" src="assets/images/Layer2.jpg" alt=""/></div>
              <div class="popular__item js-title"><span class="popular__service">Стройконтроль за работами в области инженерных сетей</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer1.png)"><img class="popular__content-pic" src="assets/images/Layer1.png" alt=""/></div>
              <div class="popular__item js-title"><span class="popular__service">Организация строительства объектов инженерного обеспечения</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
                <div class="popular__arrow js-arrow"></div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.51 75" style="fill: #f6f7f8;; width: 25px; height: 100%;" preserveAspectRatio="none"><title>triangle</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M0,0,21.51,37.5,0,75Z"/></g></g></svg>
              </div>
              <div class="popular__content js-content" style="background-image: url(assets/images/Layer3.png)"><img class="popular__content-pic" src="assets/images/Layer3.png" alt=""/></div>
            </div>
            <div class="popular__column">
              <div class="popular__bg" style="background-image: url(assets/images/Layer1.png)"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="partners pos-r">
        <div class="grid">
          <div class="container">
            <div class="grid__inner">
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
            </div>
          </div>
        </div>
        <div class="container pos-r grid-front">
          <div class="partners__title title js-animate animated" data-anim="fadeInUp"><span class="title__text_bg">Партнеры</span></div>
          <div class="partners__inner js-scroll js-animate animated" data-anim="fadeInUp">
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p1.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p2.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p3.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p4.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p5.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p1.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p2.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p3.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p4.png" alt=""/></div>
            </div>
            <div class="partners__item">
              <div class="partners__wrap"><img class="partners__pic" src="assets/images/p5.png" alt=""/></div>
            </div>
          </div>
          <div class="partners__inner partners__inner_mobile js-scroll">
            <div class="partners__row">
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p1.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p2.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p3.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p4.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p5.png" alt=""/></div>
              </div>
            </div>
            <div class="partners__row">
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p3.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p4.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p5.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p2.png" alt=""/></div>
              </div>
              <div class="partners__item">
                <div class="partners__wrap"><img class="partners__pic" src="assets/images/p1.png" alt=""/></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"news", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "brain",
		"IBLOCK_ID" => "31",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "news",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>
      <div class="gallery pos-r">
        <div class="grid">
          <div class="container">
            <div class="grid__inner">
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
            </div>
          </div>
        </div>
        <div class="container pos-r grid-front">
          <div class="gallery__row js-animate animated" data-anim="fadeInUp">
            <div class="gallery__title title"><span class="title__text_bg">Фотогалерея</span></div>
            <div class="gallery__wrapper">
              <div class="gallery__button button-border"><a class="gallery__btn button" href="#" target="_blank">Все фото</a><span class="button-border__top-left"></span><span class="button-border__top-right"></span><span class="button-border__left"></span><span class="button-border__right"></span><span class="button-border__bottom-left"></span><span class="button-border__bottom-right"></span></div>
            </div>
          </div>
          <div class="gallery__wrap js-animate animated" data-anim="fadeInUp">
            <div class="js-slick">
              <div class="gallery__inner">
                <div class="gallery__item" style="background-image: url(assets/images/Layer3.png)"></div>
              </div>
              <div class="gallery__inner">
                <div class="gallery__item" style="background-image: url(assets/images/Layer3.png)"></div>
              </div>
              <div class="gallery__inner">
                <div class="gallery__item" style="background-image: url(assets/images/Layer3.png)"></div>
              </div>
            </div>
            <div class="slick-counter">
              <div class="slick-counter__inner"><span class="js-slider-counter-current">00</span><span>/</span><span class="s-slider-counter-all">00 </span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <div class="container">
          <div class="footer__wrap"><a class="footer__logo" href="#" target="_blank"><span class="footer__logo-wrap"><img class="footer__pic-logo" src="assets/images/SVG/logo_footer.svg" alt=""/></span></a></div>
          <div class="footer__inner">
            <div class="footer__column footer__column_one">
              <div class="footer__wrapper footer__wrapper_none"><span class="footer__logo-wrap"><a class="footer__logo" href="#" target="_blank"><img class="footer__pic-logo" src="assets/images/SVG/logo_footer.svg" alt=""/></a></span></div><span class="footer__title footer__title_contact">Контакты</span>
              <div class="footer__contact">
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/phone.svg" alt=""/></div><a href="tel:+79206220001"><span class="footer__contact-text">+ 7 920 622 00 01</span></a>
                </div>
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/mess.svg" alt=""/></div><a class="footer__contact-text" href="mailto:brain-company@mail.ru" target="_blank">brain-company@mail.ru</a>
                </div>
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/map.svg" alt=""/></div><span class="footer__contact-text">г. Владимир, пр-кт Строителей, 1А</span>
                </div>
              </div>
              <div class="footer__social social"><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/vk.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/facebook55.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/twitter1.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/inst.png" alt=""/></a></div>
            </div>
            <div class="footer__column-wrapper js-footer-wrapper">
              <div class="footer__column footer__column_bgcolor footer__column_flex"><span class="footer__title js-footer-tab">О компании</span>
                <div class="footer__nav js-footer-nav">
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">О компании</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Новости</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Контакты</a></div>
                </div>
              </div>
              <div class="footer__column footer__column_flex"><span class="footer__title js-footer-tab">Услуги</span>
                <div class="footer__nav js-footer-nav">
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Строительно-монтажные работы</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Приборы учета тепловой энергии</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Проектирование инженерных систем</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Диспетчеризация</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Техническое обслуживание</a></div>
                  <div class="footer__nav-item"><a class="footer__nav-link" href="#" target="_blank">Пуско-наладочные работы</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">
          <div class="container">
            <div class="copyright__inner">
              <div class="copyright__item"><span>© ООО Инженерная компания «Брэйн», 2019</span></div>
              <div class="copyright__item"><a class="copyright__link" href="#" target="_blank">Политика конфиденциальности</a></div>
              <div class="copyright__item"><a class="copyright__logo logo" href="#" target="_blank"><img class="copyright__pic" src="assets/images/logohb.png" alt=""/><span class="logo__text">Разработка сайта</span></a><span class="copyright__text">Сайт создан в <a class="copyright__link" href="#" target="_blank">Hawking Brothers</a></span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="popup mfp-hide" id="popup">
        <div class="popup__bg js-popup-outside"></div>
        <div class="popup__inner">
          <div class="popup__close js-popup-close"></div>
          <div class="popup__content">
            <div class="popup__title">Заказать звонок</div>
            <form class="form js-form" data-validate="data-validate" autocomplete="off">
              <div class="form__field">
                <label>Имя, Фамилия, Отчество</label>
                <input class="form__inp js-input-name js-inp" type="text" name="name" required="required"/>
              </div>
              <div class="form__field">
                <label>Телефон</label>
                <input class="form__inp js-input-phone js-inp" type="text" name="tel" required="required"/>
              </div>
              <label>Время звонка</label>
              <div class="form__row">
                <div class="form__field">
                  <div class="form__check">
                    <input class="form__inp" id="time1" type="radio" name="time" checked="checked"/>
                    <label for="time1"><span class="form__check-icon"></span><span class="form__check-text">С 8.00 до 13.00</span></label>
                  </div>
                </div>
                <div class="form__field">
                  <div class="form__check">
                    <input class="form__inp" id="time2" type="radio" name="time"/>
                    <label for="time2"><span class="form__check-icon"></span><span class="form__check-text">С 8.00 до 13.00</span></label>
                  </div>
                </div>
              </div>
              <div class="form__field">
                <label>Интересующий вопрос</label>
                <textarea class="form__inp js-inp js-autosize" type="text" name="message" required="required"></textarea>
              </div>
              <div class="form__btn"> 
                <button class="form__button detalied__btn">Отправить</button>
              </div>
            </form>
          </div>
        </div>
      </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>