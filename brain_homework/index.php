<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  $APPLICATION->SetPageProperty("title", "Brain_homework");
  $APPLICATION->SetTitle("Brain_homework");
  $APPLICATION->SetPageProperty("CLASS", "main");
?><div class="general">
	<div class="general__overlay" style="background-image: url(assets/images/video.png)">
	</div>
 <video loop="" muted="" playsinline="" autoplay="" poster="assets/images/video.png" class="general__video"> <source src="assets/video/Downhill.mp4" type="video/mp4"> <source src="assets/video/Downhill.webm" type="video/webm"> </video>
	<div class="container">
		<div class="general__inner js-animate animated" data-anim="fadeInUp">
			<div class="general__content">
 <span class="border-left"> </span><span class="border-top"></span><span class="border-bottom"></span>
				<div class="general__text">
					<p>
						 Инженерная компания
					</p>
					<p>
						 совершенства систем
					</p>
					<p>
						 проектировки
					</p>
				</div>
			</div>
			<div class="general__description">
				<p>
					 Опыт воплощенный в качестве
				</p>
				<p>
					 работы любой сложности
				</p>
			</div>
		</div>
	</div>
</div>
<div class="promo pos-r">
	<div class="container">
		<div class="promo__inner js-animate animated" data-anim="fadeInUp">
			<div class="promo__img">
				<div class="promo__puls js-puls">
				</div>
				<div class="promo__puls js-puls">
				</div>
				<div class="promo__puls js-puls">
				</div>
				<div class="promo__puls js-puls">
				</div>
 <img src="assets/images/brain.png" class="promo__pic" alt="">
			</div>
			<div class="promo__content">
				<div class="promo__title title title_block">
					 Краткого текста<span class="title__text_bg">заголовок</span>
				</div>
				<div class="promo__description">
					<p>
						 Здесь может быть какая-нибудь цитата.
					</p>
					<p>
						 Все выполняемые работы сопровождаются оформлением технической документации. По завершении cтроительно монтажных работ комплект сполнительно-технической документации (ИТД) утверждается в ГРО, Ростехнадзоре и передается Заказчику для дальнейшего оформления в собственность и ввода объекта в эксплуатацию.
					</p>
				</div>
				<div class="promo__button button-border">
 <a class="button" href="#" target="_blank">Подробнее</a><span class="button-border__top-left"></span><span class="button-border__top-right"></span><span class="button-border__left"></span><span class="button-border__right"></span><span class="button-border__bottom-left"></span><span class="button-border__bottom-right"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="service-section pos-r">
	<div class="grid">
		<div class="container">
			<div class="grid__inner">
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="service-section__title title js-animate animated" data-anim="fadeInUp">
			 Наши<span class="title__text_bg">услуги</span>
		</div>
		<div class="service pos-r grid-front js-animate animated" data-anim="fadeInUp">
 <a class="service__item service__item_color1" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img src="assets/images/stroit.svg" class="service__pic" alt=""></span><span class="service__description">Строительно-монтажные работы</span></span></a><a class="service__item service__item_color2" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img src="assets/images/proekt.svg" class="service__pic" alt=""></span><span class="service__description">Проектирование инженерных систем </span></span></a><a class="service__item service__item_color3" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img src="assets/images/priboru.svg" class="service__pic" alt=""></span><span class="service__description">Приборы учета тепловой энергии </span></span></a><a class="service__item service__item_color4" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img src="assets/images/to_pusk.svg" class="service__pic" alt=""></span><span class="service__description">Строительно-монтажные работы </span></span></a><a class="service__item service__item_color5" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner"><span class="service__wrapper"><img src="assets/images/proizvodstv.svg" class="service__pic" alt=""></span><span class="service__description">Техническое обслуживание Пуско-наладочные работы</span></span></a><a class="service__item service__item_color6" href="#" target="_blank" style="background-image: url(assets/images/Layer6.png)"><span class="service__item-inner service__item-inner_bottom"><span class="service__wrapper"><img src="assets/images/dispet4.svg" class="service__pic" alt=""></span><span class="service__description">Диспетчеризация</span></span></a>
		</div>
	</div>
</div>
<div class="popular pos-r">
	<div class="grid grid_invert">
		<div class="container">
			<div class="grid__inner">
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
			</div>
		</div>
	</div>
	<div class="container pos-r grid-front">
		<div class="popular__title title js-animate animated" data-anim="fadeInUp">
 <span class="title__text_bg">Популярные</span><span class="title__text_color">услуги</span>
		</div>
		<div class="popular__inner js-wrapper js-animate animated" data-anim="fadeInUp">
			<div class="popular__column">
				<div class="popular__item js-title">
 <span class="popular__service">Строительно-монтажные работы</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer1.png)">
 <img src="assets/images/Layer1.png" class="popular__content-pic" alt="">
				</div>
				<div class="popular__item js-title">
 <span class="popular__service">Проектирование инженерных систем</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer2.jpg)">
 <img src="assets/images/Layer2.jpg" class="popular__content-pic" alt="">
				</div>
				<div class="popular__item js-title">
 <span class="popular__service">Приборы учета тепловой энергии</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer3.png)">
 <img src="assets/images/Layer3.png" class="popular__content-pic" alt="">
				</div>
				<div class="popular__item js-title">
 <span class="popular__service">Стройконтроль за работами в области инженерных сетей</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer2.jpg)">
 <img src="assets/images/Layer2.jpg" class="popular__content-pic" alt="">
				</div>
				<div class="popular__item js-title">
 <span class="popular__service">Стройконтроль за работами в области инженерных сетей</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer1.png)">
 <img src="assets/images/Layer1.png" class="popular__content-pic" alt="">
				</div>
				<div class="popular__item js-title">
 <span class="popular__service">Организация строительства объектов инженерного обеспечения</span><a class="js-click popular__btn js-button" href="#" target="_blank">Заказать</a>
					<div class="popular__arrow js-arrow">
					</div>
				</div>
				<div class="popular__content js-content" style="background-image: url(assets/images/Layer3.png)">
 <img src="assets/images/Layer3.png" class="popular__content-pic" alt="">
				</div>
			</div>
			<div class="popular__column">
				<div class="popular__bg" style="background-image: url(assets/images/Layer1.png)">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="partners pos-r">
	<div class="grid">
		<div class="container">
			<div class="grid__inner">
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
			</div>
		</div>
	</div>
	<div class="container pos-r grid-front">
		<div class="partners__title title js-animate animated" data-anim="fadeInUp">
 <span class="title__text_bg">Партнеры</span>
		</div>
		<div class="partners__inner js-scroll js-animate animated" data-anim="fadeInUp">
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p1.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p2.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p3.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p4.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p5.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p1.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p2.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p3.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p4.png" class="partners__pic" alt="">
				</div>
			</div>
			<div class="partners__item">
				<div class="partners__wrap">
 <img src="assets/images/p5.png" class="partners__pic" alt="">
				</div>
			</div>
		</div>
		<div class="partners__inner partners__inner_mobile js-scroll">
			<div class="partners__row">
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p1.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p2.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p3.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p4.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p5.png" class="partners__pic" alt="">
					</div>
				</div>
			</div>
			<div class="partners__row">
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p3.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p4.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p5.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p2.png" class="partners__pic" alt="">
					</div>
				</div>
				<div class="partners__item">
					<div class="partners__wrap">
 <img src="assets/images/p1.png" class="partners__pic" alt="">
					</div>
					<div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"news", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j M Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "31",
		"IBLOCK_TYPE" => "brain",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "j M Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "DATE",
			1 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "news",
		"SEF_FOLDER" => "/brain_homework/",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "news/#ELEMENT_CODE#/",
		)
	),
	false
);?>

<div class="gallery pos-r">
	<div class="grid">
		<div class="container">
			<div class="grid__inner">
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
				<div class="grid__item">
				</div>
			</div>
		</div>
	</div>
	<div class="container pos-r grid-front">
		<div class="gallery__row js-animate animated" data-anim="fadeInUp">
			<div class="gallery__title title">
 <span class="title__text_bg">Фотогалерея</span>
			</div>
			<div class="gallery__wrapper">
				<div class="gallery__button button-border">
 <a class="gallery__btn button" href="#" target="_blank">Все фото</a><span class="button-border__top-left"></span><span class="button-border__top-right"></span><span class="button-border__left"></span><span class="button-border__right"></span><span class="button-border__bottom-left"></span><span class="button-border__bottom-right"></span>
				</div>
			</div>
		</div>
		<div class="gallery__wrap js-animate animated" data-anim="fadeInUp">
			<div class="js-slick">
				<div class="gallery__inner">
					<div class="gallery__item" style="background-image: url(assets/images/Layer3.png)">
					</div>
				</div>
				<div class="gallery__inner">
					<div class="gallery__item" style="background-image: url(assets/images/Layer3.png)">
					</div>
				</div>
				<div class="gallery__inner">
					<div class="gallery__item" style="background-image: url(assets/images/Layer3.png)">
					</div>
				</div>
			</div>
			<div class="slick-counter">
				<div class="slick-counter__inner">
 <span class="js-slider-counter-current">00</span>/<span class="s-slider-counter-all">00 </span>
				</div>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>