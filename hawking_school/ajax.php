<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
    Bitrix\Main\Context;

Loader::includeModule("iblock");

$request = Context::getCurrent()->getRequest()->getPostList();

$el = new CIBlockElement;

$PROP = [ //Запись данных, которые пришли в REQUEST, в массив PROP 
    'AGE' => $request['form']['age'],
    'EDUCATION' => $request['form']['education'],
    'ACHIEVMENTS' => $request['form']['achievements'],
    'EXP' => $request['form']['exp'],
    'EXAMPLE' => $request['form']['example'],
    'TEST' => $request['form']['test'],
    'STUDY' => $request['form']['study'],
    'REASONS' => $request['form']['reasons'],
    'PHONE' => $request['form']['phone'],
    'EMAIL' => $request['form']['email'],

];

$arLoadProductArray = [
    "IBLOCK_ID"      => 30, //идентификатор инфоблока
    "PROPERTY_VALUES" => $PROP, //свойства инфоблока
    "NAME"           => $request['form']['name'], //название элемента инфоблока
    "ACTIVE"         => "Y",            // активен
    "PREVIEW_TEXT"   => implode("\n\r", $request['form'])
];

if ($PRODUCT_ID = $el->Add($arLoadProductArray)) { //если удалось записать в инфоблок
    echo json_encode(
        [
            'success' => true,
            'message' => $request
            ]
        ); //сообщение об успешком выполнении

    \Bitrix\Main\Mail\Event::sendImmediate(array(  //отправка письма
        "EVENT_NAME" => "FEEDBACK",  //название типа почтового события
        "LID" => "s1", //сайт
        "C_FIELDS" => array( //свойства шаблона почтового события
            'CREATED_BY_EMAIL' => "pashokkov@mail.ru",
            'ADMIN_EMAIL' => "pashokkov@mail.ru",
            'NAME' => $request['form']['name'],
            'AGE' => $request['form']['age'],
            'EDUCATION' => $request['form']['education'],
            'ACHIEVMENTS' => $request['form']['achievements'],
            'EXP' => $request['form']['exp'],
            'EXAMPLE' => $request['form']['example'],
            'TEST' => $request['form']['test'],
            'STUDY' => $request['form']['study'],
            'REASONS' => $request['form']['reasons'],
            'PHONE' => $request['form']['phone'],
            'EMAIL' => $request['form']['email'],
        ),
    ));
} else {
    //если не удалось записать в инфоблок
    echo json_encode(['success' => false, 'message' => $el->LAST_ERROR]); //сообщение об ошибке
}
