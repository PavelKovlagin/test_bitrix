<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
  $APPLICATION->SetPageProperty("title", "Hawking School");
  $APPLICATION->SetTitle("Hawking School");
  $APPLICATION->SetPageProperty("CLASS", "main");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"main", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_HANDLERS" => array(
			0 => "delicious",
		),
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "21",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => "main",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
		"DETAIL_URL" => "",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"GROUP_PERMISSIONS" => array(
			0 => "1",
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"STRICT_SECTION_CHECK" => "Y",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "main",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"directions", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "22",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "directions",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"skills", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "24",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "skills",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"awaits", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "25",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "awaits",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"process", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "26",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "process",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"join", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "27",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "join",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"feedback", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "28",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "feedback",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>


<div class="section section_form" id="form">
	<div class="container">
		<h2 class="h2">Заполни анкету</h2>
		<div class="form">
			<div class="form__arrows form__arrows_left js-form-arrows" data-direction="left"></div>
			<div class="form__arrows form__arrows_right js-form-arrows" data-direction="right"></div>
			<div class="form__main">
				<div class="form__panel-pic"><svg xmlns="http://www.w3.org/2000/svg" width="750" height="44"
						viewBox="0 0 750 44" fill="none">
						<path
							d="M1 1L41.2846 42.2438C41.9111 42.8852 43 42.4416 43 41.545V3.45496C43 2.55837 44.0889 2.11482 44.7154 2.75622L83.2846 42.2438C83.9111 42.8852 85 42.4416 85 41.545V3.45496C85 2.55837 86.0889 2.11482 86.7154 2.75622L125.285 42.2438C125.911 42.8852 127 42.4416 127 41.545V3.45496C127 2.55837 128.089 2.11482 128.715 2.75622L167.285 42.2438C167.911 42.8852 169 42.4416 169 41.545V3.45496C169 2.55837 170.089 2.11482 170.715 2.75622L209.285 42.2438C209.911 42.8852 211 42.4416 211 41.545V3.45496C211 2.55837 212.089 2.11482 212.715 2.75622L251.285 42.2438C251.911 42.8852 253 42.4416 253 41.545V3.45496C253 2.55837 254.089 2.11482 254.715 2.75622L293.285 42.2438C293.911 42.8852 295 42.4416 295 41.545V3.45496C295 2.55837 296.089 2.11482 296.715 2.75622L335.285 42.2438C335.911 42.8852 337 42.4416 337 41.545V3.45496C337 2.55837 338.089 2.11482 338.715 2.75622L377.285 42.2438C377.911 42.8852 379 42.4416 379 41.545V3.45496C379 2.55837 380.089 2.11482 380.715 2.75622L419.285 42.2438C419.911 42.8852 421 42.4416 421 41.545V3.45496C421 2.55837 422.089 2.11482 422.715 2.75622L461.285 42.2438C461.911 42.8852 463 42.4416 463 41.545V3.45496C463 2.55837 464.089 2.11482 464.715 2.75622L503.285 42.2438C503.911 42.8852 505 42.4416 505 41.545V3.45496C505 2.55837 506.089 2.11482 506.715 2.75622L545.285 42.2438C545.911 42.8852 547 42.4416 547 41.545V3.45496C547 2.55837 548.089 2.11482 548.715 2.75622L587.285 42.2438C587.911 42.8852 589 42.4416 589 41.545V3.45496C589 2.55837 590.089 2.11482 590.715 2.75622L629.285 42.2438C629.911 42.8852 631 42.4416 631 41.545V3.45496C631 2.55837 632.089 2.11482 632.715 2.75622L671.285 42.2438C671.911 42.8852 673 42.4416 673 41.545V3.45496C673 2.55837 674.089 2.11482 674.715 2.75622L713.285 42.2438C713.911 42.8852 715 42.4416 715 41.545V3.4808C715 2.58068 716.096 2.1388 716.72 2.78729L749 36.3209"
							stroke="white" /></svg></div>
				<form class="form__main-wrap" name="feedback">
					<div class="form__step-wrap js-form-step-wrap is-active">
						<div class="form__steps"><span class="form__steps-title">Шаг 1 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<input class="form__input js-input-name" type="text" required="required"
									placeholder="Имя Фамилия" name="name" />
								<input class="form__input js-input-number" type="text" required="required"
									placeholder="Полных лет" name="age" />
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev" disabled="disabled">
								<span>Назад</span></div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 2 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area" required="required"
									placeholder="Образование (университет, специальность, курс)"
									name="education"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 3 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area" required="required"
									placeholder="Расскажи о самом главном достижении за последний год (любом: учебном, личном)"
									name="achievements"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 4 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area form__area_type_middle" required="required"
									placeholder="Опыт программирования" name="exp"></textarea>
								<textarea class="form__area form__area_type_small" required="required"
									placeholder="Ссылка на пример кода" name="example"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 5 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area" required="required"
									placeholder="Тебе дали задачу, но ты совершенно не знаешь, как ее решать. С чего начнешь выполнение?"
									name="test"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 6 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area" required="required"
									placeholder="Что изучил за последний год (не обязательно по работе)?"
									name="study"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 7 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<textarea class="form__area" required="required"
									placeholder="Почему хочешь стажироваться у нас?" name="reasons"></textarea>
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_prev js-form-button-prev"><span>Назад</span>
							</div>
							<div class="form__button form__button_next js-form-button-next"><span>Вперед</span>
							</div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Шаг 8 из 9</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__fields js-form-fields">
								<input class="form__input js-input-phone" type="text" required="required"
									placeholder="Телефон" name="phone" />
								<input class="form__input js-input-email" required="required"
									placeholder="Email" name="email" />
							</div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_next js-form-button-submit" type="submit">
								<span>Отправить</span></div>
						</div>
					</div>
					<div class="form__step-wrap js-form-step-wrap">
						<div class="form__steps"><span class="form__steps-title">Спасибо!</span>
							<div class="form__steps-line">
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
								<div class="is-active"></div>
							</div>
						</div>
						<div class="form__inner">
							<div class="form__message"><img src="assets/images/form-covert.svg"
									alt="" /><span><strong>Твоя заявка отправлена.</strong><br /> В&nbsp;течение
									недели мы&nbsp;с&nbsp;тобой свяжемся</span></div>
						</div>
						<div class="form__buttons">
							<div class="form__button form__button_reset js-form-button-reset"><span>К
									началу</span></div>
						</div>
					</div>
				</form>
				<div class="form__panel-msg text"><span>Отправляя заявку, я&nbsp;соглашаюсь на&nbsp;обработку <a
							href="{{setting('hb-admins-path.POLITICAL')}}" target="_blank">персональных
							данных</a></span></div>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"faq", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "29",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "faq",
		"TEMPLATE_THEME" => "blue",
		"STRICT_SECTION_CHECK" => "N",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N",
		"FILE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"map", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_HANDLERS" => array(
			0 => "delicious",
		),
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "23",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => "map",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
		"DETAIL_URL" => "",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"GROUP_PERMISSIONS" => array(
			0 => "1",
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"STRICT_SECTION_CHECK" => "Y",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "map",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILE_404" => ""
	),
	false
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>