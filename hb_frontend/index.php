<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("HB_Frontend");
?>

<div class="wrapper">

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"bootstrap_v4", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "N",
		"COMPONENT_TEMPLATE" => "bootstrap_v4",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "trendFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "references",
		"IBLOCK_TYPE_ID" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
			3 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "12",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => "",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "NEWPRODUCT",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "site",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"BACKGROUND_IMAGE" => "-",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"ENLARGE_PRODUCT" => "STRICT",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"LABEL_PROP_MOBILE" => "",
		"LABEL_PROP_POSITION" => "top-left",
		"SHOW_MAX_QUANTITY" => "N",
		"RCM_TYPE" => "personal",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"SHOW_FROM_SECTION" => "N",
		"DISPLAY_COMPARE" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"LAZY_LOAD" => "N",
		"LOAD_ON_SCROLL" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);?>

        <header class="header js-header">
            <div class="container">
                <div class="header_inner">
                    <a class="logo" href="#">
                        <img class="logo_orange" src="<?=SITE_TEMPLATE_PATH ?>/images/header/logo header.png" alt="logo" />
                        <img class="logo_white" src="<?=SITE_TEMPLATE_PATH ?>/images/logo_fixed_header_footer/logo1.png" alt="logo" />
                    </a>
                    <div class="header_group">
                        <div class="burger js-burger">
                            <div class="burger_line"></div>
                            <div class="burger_line"></div>
                            <div class="burger_line"></div>
                        </div>
                        <div class="header-dropdown js-header-dropdown">
                            
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu", 
							"hb_menu", 
							array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "top",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "bino_menu",
							"MENU_THEME" => "blue"
							),
							false
						);
						?>


                            <div class="social">
                                <a href="https://facebook.com" class="social_item" target="_blank">
                                    <img class="fb" src="<?=SITE_TEMPLATE_PATH ?>/images/header/fb-icon.png" alt="" />
                                    <img class="fb-fixed" src="<?=SITE_TEMPLATE_PATH ?>/images/Fixed_Header/fb-icon_fixed_header.png" alt="" />
                                    <img class="fb_hover" src="<?=SITE_TEMPLATE_PATH ?>/images/social_hover/fb-icon.png" alt="" />
                                </a>
                                <a href="https://vk.com" class="social_item" target="_blank">
                                    <img class="vk" src="<?=SITE_TEMPLATE_PATH ?>/images/header/vk-icon.png" alt="" />
                                    <img class="vk-fixed" src="<?=SITE_TEMPLATE_PATH ?>/images/Fixed_Header/vk-icon_fixed_header.png" alt="" />
                                    <img class="vk_hover" src="<?=SITE_TEMPLATE_PATH ?>/images/social_hover/vk-icon.png" alt="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="main js-sectionfirst">
            <div class="main_layer">
                <div class="container">
                    <div class="carousel carousel-js">
                        <div class="main_content_inner">
                            <div class="main_content">
                                <h1 class="main_title">Mountains</h1>
                                <p class="main_text">Текст баннера может включать в себя несколько предложений.
                                    Выравнивание по
                                    центру. Ширина текстового блока на десктопе 850px</p>
                                <a href="#" class="main_button">Оставить заявку</a>
                            </div>
                        </div>
                        <div class="main_content_inner">
                            <div class="main_content">
                                <h1 class="main_title">Mountains</h1>                                
                                <p class="main_text">Текст баннера может включать в себя несколько предложений.
                                    Выравнивание по
                                    центру. Ширина текстового блока на планшете 600px</p>
                                <a href="#" class="main_button">Оставить заявку</a>
                            </div>
                        </div>
                        <div class="main_content_inner">
                            <div class="main_content">
                                <h1 class="main_title">Mountains</h1>
                                <p class="main_text">Текст баннера может включать в себя несколько предложений.
                                    Выравнивание по
                                    центру. Ширина текстового блока на телефоне 280px</p>
                                <a href="#" class="main_button">Оставить заявку</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="description" id="about">
            <div class="container">
                <div class="description-inner">
                    <div class="description-group">
                        <h2 class="description_title"><a href="lesson1/index.php">Урок 1</a></h2>
                        <h2 class="description_title"><a href="lesson2/index.php">Урок 2</a></h2>
                        <h2 class="description_title"><a href="lesson3/index.php">Урок 3</a></h2>                        
                        <h2 class="description_title">Заголовок</h2>                        
                        <p class="description_text">Таким образом дальнейшее развитие различных форм деятельности в
                            значительной степени обуславливает создание системы обучения кадров, соответствует насущным
                            потребностям. Задача организации, в особенности же рамки и место обучения кадров в
                            значительной степени обуславливает создание системы обучения кадров, соответствует.</p>
                    </div>
                    <div class="columns">
                        <div class="column"></div>
                        <div class="column_first"></div>
                        <div class="column_second"></div>
                        <div class="column_third"></div>
                        <div class="column_fourth"></div>
                    </div>
                    <img class="description_image" src="<?=SITE_TEMPLATE_PATH ?>/images/about/imagen_about.png" alt="" />
                </div>
            </div>
        </div>
        <div class="offers" id="offer">
            <div class="container">
                <h2 class="offers_title">Наши Предложения</h2>
            </div>
        </div>
        <div class="accordion">
            <div class="container">
                <div class="accordion_item">
                    <div class="accordion_item_head js-accordion-item-head">
                        <div class="accordion_icon"></div>
                        <span class="accordion_title">Предложение 1. 10 дней.</span>
                    </div>

                    <div class="accordion_item_content js-accordion-item-content">
                        <div class="accordion_item_content_inner">
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Город:</span>
                                    <span>Город</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Цена:</span>
                                    <span>250 000 ₽</span>
                                </div>
                            </div>
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Отель:</span>
                                    <span>Отель</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Авиакомпания:</span>
                                    <span>Авиакомпания</span>
                                </div>
                            </div>
                            <div class="accordion_button_item">
                                <a href="#" class="accordion_button">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion_item is-active">
                    <div class="accordion_item_head js-accordion-item-head">
                        <div class="accordion_icon"></div>
                        <span class="accordion_title">Предложение 2. 7 дней.</span>
                    </div>

                    <div class="accordion_item_content js-accordion-item-content" style="max-height: none;">
                        <div class="accordion_item_content_inner">
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Город:</span>
                                    <span>Город</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Цена:</span>
                                    <span>250 000 ₽</span>
                                </div>
                            </div>
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Отель:</span>
                                    <span>Отель</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Авиакомпания:</span>
                                    <span>Авиакомпания</span>
                                </div>
                            </div>
                            <div class="accordion_button_item">
                                <a href="#" class="accordion_button">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion_item">
                    <div class="accordion_item_head js-accordion-item-head">
                        <div class="accordion_icon"></div>
                        <span class="accordion_title">Предложение 3. 14 дней.</span>
                    </div>

                    <div class="accordion_item_content js-accordion-item-content">
                        <div class="accordion_item_content_inner">
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Город:</span>
                                    <span>Город</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Цена:</span>
                                    <span>250 000 ₽</span>
                                </div>
                            </div>
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Отель:</span>
                                    <span>Отель</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Авиакомпания:</span>
                                    <span>Авиакомпания</span>
                                </div>
                            </div>
                            <div class="accordion_button_item">
                                <a href="#" class="accordion_button">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion_item">
                    <div class="accordion_item_head js-accordion-item-head">
                        <div class="accordion_icon"></div>
                        <span class="accordion_title">Предложение 4. 8 дней.</span>
                    </div>

                    <div class="accordion_item_content js-accordion-item-content">
                        <div class="accordion_item_content_inner">
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Город:</span>
                                    <span>Город</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Цена:</span>
                                    <span>250 000 ₽</span>
                                </div>
                            </div>
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Отель:</span>
                                    <span>Отель</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Авиакомпания:</span>
                                    <span>Авиакомпания</span>
                                </div>
                            </div>
                            <div class="accordion_button_item">
                                <a href="#" class="accordion_button">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion_item">
                    <div class="accordion_item_head js-accordion-item-head">
                        <div class="accordion_icon"></div>
                        <span class="accordion_title">Предложение 5. 3 дня.</span>
                    </div>

                    <div class="accordion_item_content js-accordion-item-content">
                        <div class="accordion_item_content_inner">
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Город:</span>
                                    <span>Город</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Цена:</span>
                                    <span>250 000 ₽</span>
                                </div>
                            </div>
                            <div class="accordion_wrap">
                                <div class="accordion_text">
                                    <span>Отель:</span>
                                    <span>Отель</span>
                                </div>
                                <div class="accordion_text">
                                    <span>Авиакомпания:</span>
                                    <span>Авиакомпания</span>
                                </div>
                            </div>
                            <div class="accordion_button_item">
                                <a href="#" class="accordion_button">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="callback">
            <div class="container">
                <h2 class="callback_title">Оставьте заявку</h2>
                <div class="callback_inner">
                    <form action="" class="form form-js">
                        <label for="text" class="form_field">
                            <span class="form_label">Имя:</span>
                            <input type="text" id="text" name="text" class="form_input input-name-js" placeholder="Введите имя" required />
                        </label>
                        <label for="phone" class="form_field">
                            <span class="form_label">Телефон:</span>
                            <input type="tel" id="phone" name="phone" class="form_input phone-js" placeholder="+7 (___) ___-__-__" required />
                        </label>
                        <label for="email" class="form_field">
                            <span class="form_label">E-mail:</span>
                            <input type="email" id="email" name="email" class="form_input email-js" placeholder="example@mail.com" required />
                        </label>
                        <label for="form_select" class="form_field">
                            <span class="form_label">Выберите предложение:</span>
                            <select class="form_select select-js" id="form_select" name="option">
                                <option value="Предложение 1">Предложение 1</option>
                                <option value="Предложение 2">Предложение 2</option>
                                <option value="Предложение 3">Предложение 3</option>
                            </select>
                        </label>
                        <div class="form_field">
                            <span class="form_label">Пол:</span>
                            <label class="form_radio">
                                <input type="radio" name="radio" value="Мужской" checked />
                                <span>Мужской</span>
                            </label>
                            <label class="form_radio">
                                <input type="radio" name="radio" value="Женский" />
                                <span>Женский</span>
                            </label>
                        </div>
                        <label class="form_field">
                            <span class="form_label">Комментарий:</span>
                            <textarea class="form_input area" placeholder="Введите комментарий" required></textarea>
                        </label>
                        <label class="form_checkbox">
                            <input type="checkbox" name="checkbox" checked required/>
                            <span class="checkbox_text">Я даю согласие на обработку персональных данных</span>
                        </label>
                        <div class="form_button_item">
                            <button type="submit" class="form_button">Отправить</button>
                        </div>
                    </form>
                    

                </div>

            </div>

        </div>
        <div class="contacts" id="contact">
            <div class="container">
                <div class="cantacts__inner">
                    <div class="contacts_group">
                        <h2 class="contacts_title">Контакты</h2>
                        <div class="contacts_item">
                            <img class="contacts_icon" src="<?=SITE_TEMPLATE_PATH ?>/images/contacts/location-svg.png" alt="">
                            <span class="contacts_text">ул. Арбат, 1а, Москва
                                119019, Россия</span>
                        </div>
                        <div class="contacts_item">
                            <img class="contacts_icon" src="<?=SITE_TEMPLATE_PATH ?>/images/contacts/phone-svg.png" alt="">
                            <span class="contacts_text">+7 (495) 000-00-00</span>
                        </div>
                        <div class="contacts_item">
                            <img class="contacts_icon" src="<?=SITE_TEMPLATE_PATH ?>/images/contacts/email-svg.png" alt="">
                            <span class="contacts_text">info@mysite.ru</span>
                        </div>
                    </div>
                    <div id="map" class="map"></div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <a href="#"><img class="footer_logo" src="<?=SITE_TEMPLATE_PATH ?>/images/logo_fixed_header_footer/logo1.png" alt="" /></a>
                    <div class="footer_group">
                        <div class="footer_copyright">
                            <span class="footer_text">© 2020 Mountains. <br />
                                Сайт создан в Hawking Brothers.</span>
                        </div>
                        <div class="social_footer">
                            <a href="#" class="footer_item">
                                <img class="fb_footer" src="<?=SITE_TEMPLATE_PATH ?>/images/header/fb-icon.png" alt="" />
                            </a>
                            <a href="#" class="footer_item">
                                <img class="vk_footer" src="<?=SITE_TEMPLATE_PATH ?>/images/header/vk-icon.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="popup popup-js">        
        <div class="popup__content">            
            <img class="line_popup line-popup-js" src="<?=SITE_TEMPLATE_PATH ?>/images/popup/close-icon.png" alt="">
            <h2 class="popup_title">Авторизация</h2>
            <form action="" class="popup_form">
                <label for="tpopup" class="popup_form_field">
                    <span class="popup_form_lable">Логин:</span>
                    <input type="text" id="tpopup" class="popup_input popup-input-js" placeholder="Введите имя" required>
                </label>
                <label for="password" class="popup_form_field">
                    <span class="popup_form_lable">Пароль:</span>
                    <input type="password" id="password" class="popup_input popup-input-js" placeholder="Введите пароль"
                        required>
                </label>
            </form>
            <div class="popup_button_item">
                <a href="#" class="popup_button">Авторизоваться</a>
            </div>
        </div>
    </div>
    </div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>