<?
// файл /bitrix/php_interface/init.php
// регистрируем обработчик
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyClass", "OnBeforeIBlockElementAddHandler"));

class MyClass
{
    // создаем обработчик события "OnBeforeIBlockElementAdd"
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {   
        CModule::IncludeModule("iblock");
        $dbE = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => $arFields['IBLOCK_ID']));
        if ($arE = $dbE->GetNext()) { // первая запись выборки
            switch ($arE['IBLOCK_CODE']) {
                case 'form':
                    $arFields['PREVIEW_TEXT'] = json_encode($arFields);
                    //$arFields['PREVIEW_TEXT'] = 'Идентификатор инфоблока: ' . $arFields['IBLOCK_ID'] . ', Символьный код инфоблока: ' . $arFields['IBLOCK_CODE']; 
                break;
            }
        }
    }
}
?>