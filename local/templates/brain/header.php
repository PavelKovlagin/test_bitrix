<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

?>
<!DOCTYPE html>
<html>

<head>
	<title><?$APPLICATION->ShowTitle();?></title>
	<meta charset="UTF-8"/>
	<link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/assets/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/libs.min.css"/>
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main.css"/>
	
	<?$APPLICATION->ShowHead();?>
	
</head>

<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>

	<div class="wrapper">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "main_menu", Array(
	"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"COMPONENT_TEMPLATE" => "grey_tabs"
	),
	false
);?>
	
