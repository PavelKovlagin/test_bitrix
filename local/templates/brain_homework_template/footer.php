<div class="footer">

        <div class="container">
          <div class="footer__wrap"><a class="footer__logo" href="#" target="_blank"><span class="footer__logo-wrap"><img class="footer__pic-logo" src="assets/images/SVG/logo_footer.svg" alt=""/></span></a></div>
          <div class="footer__inner">
            <div class="footer__column footer__column_one">
              <div class="footer__wrapper footer__wrapper_none"><span class="footer__logo-wrap"><a class="footer__logo" href="#" target="_blank"><img class="footer__pic-logo" src="assets/images/SVG/logo_footer.svg" alt=""/></a></span></div><span class="footer__title footer__title_contact">Контакты</span>
              <div class="footer__contact">
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/phone.svg" alt=""/></div><a href="tel:+79206220001"><span class="footer__contact-text">+ 7 920 622 00 01</span></a>
                </div>
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/mess.svg" alt=""/></div><a class="footer__contact-text" href="mailto:brain-company@mail.ru" target="_blank">brain-company@mail.ru</a>
                </div>
                <div class="footer__contact-item">
                  <div class="footer__contact-wrapper"><img class="footer__pic" src="assets/images/SVG/map.svg" alt=""/></div><span class="footer__contact-text">г. Владимир, пр-кт Строителей, 1А</span>
                </div>
              </div>
              <div class="footer__social social"><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/vk.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/facebook55.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/twitter1.png" alt=""/></a><a class="social__link" href="#" target="_blank"><img class="social__pic" src="assets/images/inst.png" alt=""/></a></div>
            </div>
          
            <div class="wrapper">
              <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"bottom_menu", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "subtop",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "bottom_menu",
		"MENU_THEME" => "site"
	),
	false
);?>

          </div>
        </div>
        
        <div class="copyright">
          <div class="container">
            <div class="copyright__inner">
              <div class="copyright__item"><span>© ООО Инженерная компания «Брэйн», 2019</span></div>
              <div class="copyright__item"><a class="copyright__link" href="#" target="_blank">Политика конфиденциальности</a></div>
              <div class="copyright__item"><a class="copyright__logo logo" href="#" target="_blank"><img class="copyright__pic" src="assets/images/logohb.png" alt=""/><span class="logo__text">Разработка сайта</span></a><span class="copyright__text">Сайт создан в <a class="copyright__link" href="#" target="_blank">Hawking Brothers</a></span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="popup mfp-hide" id="popup">
        <div class="popup__bg js-popup-outside"></div>
        <div class="popup__inner">
          <div class="popup__close js-popup-close"></div>
          <div class="popup__content">
            <div class="popup__title">Заказать звонок</div>
            <form class="form js-form" data-validate="data-validate" autocomplete="off">
              <div class="form__field">
                <label>Имя, Фамилия, Отчество</label>
                <input class="form__inp js-input-name js-inp" type="text" name="name" required="required"/>
              </div>
              <div class="form__field">
                <label>Телефон</label>
                <input class="form__inp js-input-phone js-inp" type="text" name="tel" required="required"/>
              </div>
              <label>Время звонка</label>
              <div class="form__row">
                <div class="form__field">
                  <div class="form__check">
                    <input class="form__inp" id="time1" type="radio" name="time" checked="checked"/>
                    <label for="time1"><span class="form__check-icon"></span><span class="form__check-text">С 8.00 до 13.00</span></label>
                  </div>
                </div>
                <div class="form__field">
                  <div class="form__check">
                    <input class="form__inp" id="time2" type="radio" name="time"/>
                    <label for="time2"><span class="form__check-icon"></span><span class="form__check-text">С 8.00 до 13.00</span></label>
                  </div>
                </div>
              </div>
              <div class="form__field">
                <label>Интересующий вопрос</label>
                <textarea class="form__inp js-inp js-autosize" type="text" name="message" required="required"></textarea>
              </div>
              <div class="form__btn"> 
                <button class="form__button detalied__btn">Отправить</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <script src="<?= SITE_TEMPLATE_PATH ?>/js/libs.min.js"></script>
      <script src="<?= SITE_TEMPLATE_PATH ?>/js/common.js"></script>
    </div>
  </body>
</html>