<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

?>
<!DOCTYPE html>
<html>

<head>
	<title><?$APPLICATION->ShowTitle();?></title>
	<meta charset="UTF-8"/>
	<link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/assets/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/libs.min.css"/>
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main.css"/>
	
	<?$APPLICATION->ShowHead();?>
	
</head>

<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>

	<div class="wrapper">
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main_menu", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "subtop",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "main_menu",
		"MENU_THEME" => "site"
	),
	false
);?>
	
