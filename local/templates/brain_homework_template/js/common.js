var debugTimes = false;

/**
 * Global Variables
 */

/**
 * Document Ready
 */
$(document).ready(function() {
    $('.js-scroll').each(function(index, elem){
        var ps = new PerfectScrollbar(elem, {
            wheelSpeed: 2,
            wheelPropagation: true,
            minScrollbarLength: 60,
        });

        var isDrag = false;
        var lastX = 0;
        $(elem).on('mousedown', function(event) {
          isDrag = true;
          lastX = event.pageX;
        });
        $(document).on('mousemove', function(event) {
          if(isDrag) {
            var currentX = event.pageX;
            var delta = lastX - currentX;
            $(elem).scrollLeft($(elem).scrollLeft() + delta);
            lastX = currentX;
          }
        })
        $(document).on('mouseup', function() {
          isDrag = false;
        });

        $(window).on('resize', function(){
            ps.update();
        });
    })
    
    $('.js-animate').each(function(index, elem){
      var waypoint = new Waypoint({
				element: elem,
				handler: function(direction) {
					var anim = $(elem).attr('data-anim');
					$(elem).addClass(anim);
        },
        offset: '85%'
			});
    })

})


/**
 * Components
 */

$(document).on('click', '.js-header-tab', function(){
    if(window.innerWidth >= 1280){
        if($(this).hasClass('is-active')){
            $(this).siblings('.js-header-dropdown').removeClass('is-active');
            $(this).removeClass('is-active')
        } else {
            var $wrapper = $(this).parents('.js-header-wrap');
            $wrapper.find('.js-header-tab').removeClass('is-active');
            $(this).addClass('is-active');
            $wrapper.find('.js-header-dropdown').removeClass('is-active');
            $(this).siblings('.js-header-dropdown').addClass('is-active');
        }
    } else {
        $(this).toggleClass('is-active');
        $(this).siblings('.js-header-dropdown').toggleClass('is-active');
    }

    return false
});

$(document).on('click', function(event) {
    if(!$(event.target).closest('.js-header-dropdown').length && (window.innerWidth >= 1280)){
      $('.js-header-dropdown, .js-header-tab').removeClass('is-active');
    }
});

$('.js-burg').on('click', function(){
    $(this).toggleClass('is-active');
    $('.js-menu').toggleClass('is-active');
});


function counter(event, slick) {
  var $counter = slick.$slider.parent().find('.slick-counter');
  var $current = $counter.find('.js-slider-counter-current');
  var $all = $counter.find('.s-slider-counter-all');
  
  var curr = slick.currentSlide + 1;
  if(curr < 10) curr = '0'+curr;
  
  var all = slick.slideCount;
  if(all < 10) all = '0'+all;
  
  $current.text(curr)
  $all.text(all);
}

$('.js-slick').on('init reInit', counter);




$('.js-slick').slick({
    centerMode: true,
    centerPadding: '42px',
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 1279,
        settings: {
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          infinite: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          centerMode: true,
          centerPadding: '25px',
          slidesToShow: 1,
          infinite: false
        }
      }
    ]
  });

  $('.js-slick').on('swipe afterChange', counter);
$(document).on('click', '.js-footer-tab', function(){
    if(window.innerWidth <= 780){
        if($(this).hasClass('is-active')){
            $(this).siblings('.js-footer-nav').slideToggle();
            $(this).removeClass('is-active')
        } else {
            $(this).addClass('is-active');
            $(this).siblings('.js-footer-nav').slideToggle()
        }
    }
});
$(document).on('click', '.js-title', function () {
    if (window.innerWidth <= 780) {
        if ($(this).hasClass('is-active')) {
            $(this).next('.js-content').slideUp();
            $(this).removeClass('is-active');
        } else {
            $(this).removeClass('is-active');
            $(this).next('.js-content').slideDown();
            $(this).addClass('is-active');
    

        }
    }
});
$(document).on('click', '.js-slide-list', function(){
       $(this).parent('.js-parent').siblings('.js-wrap').find('.js-content').css("display", "block" )
        $(this).parent('.js-parent').hide()
});
$.extend( $.validator.messages, {
    required: "Необходимо заполнить поле.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный адрес электронной почты.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
});

$.validator.addMethod('js-input-phone', function(value, element) {
    return this.optional(element) || $(element).inputmask('unmaskedvalue').length === 10;
}, 'Введите корректный номер');


$('[data-validate]').each(function(index,element){
    $(element).validate({
        errorPlacement: function(error, element) {
            var $parent = element.parent();
            $parent.append(error);
        },
        submitHandler: function(form){
            $(form).trigger('formSubmit');
        },
        onkeyup: function(element) {
            $(element).valid(); 
        }
    });
});

$('.js-input-phone').inputmask('+7 (999) 999-99-99');

$('.js-input-name').on('input', function() {
    var value = $(this).val();
    value = value.replace(/[^a-zA-Zа-яА-Я\s\-]/ig, '');
    $(this).val(value);
});

$('.js-click, .js-nav-popup').magnificPopup({
    items: {
        src: '#popup',
        type: 'inline',
        removalDelay: 900,
    },
    callbacks: {
        beforeOpen: function(){
            var pr = window.innerWidth - $(window).width();
            var scrollTop = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
            $('body').css('padding-right', pr );
            $('.js-mail').css('margin-right', pr);
            $('.js-navigation').css('margin-right', pr);
            $('html, body').addClass('is-hidden-mobile');
            $('html, body').scrollTop(scrollTop);
        },
        beforeClose: function(){
            var scrollTop = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
            $('body').css('padding-right', '' );
            $('.js-mail').css('margin-right', '');
            $('.js-navigation').css('margin-right', '');
            $('html, body').removeClass('is-hidden-mobile');
            $('html, body').scrollTop(scrollTop);
        },
    }
});

$('.js-popup-outside, .js-popup-close').on( "click", function() {
    $.magnificPopup.close();
    
});

$('.popup-youtube').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: true,
});
$(document).ready(function() {
    $('.home').find('table').each(function(index, element){
        $(element).wrap('<div class="home__table-wrap"></div>');

        var parent = $(element).parent()[0];

        var ps = new PerfectScrollbar(parent, {
            wheelSpeed: 2,
            wheelPropagation: true,
            minScrollbarLength: 60,
        });

        var isDrag = false;
        var lastX = 0;
        $(parent).on('mousedown', function(event) {
          isDrag = true;
          lastX = event.pageX;
        });
        $(document).on('mousemove', function(event) {
          if(isDrag) {
            var currentX = event.pageX;
            var delta = lastX - currentX;
            $(parent).scrollLeft($(parent).scrollLeft() + delta);
            lastX = currentX;
          }
        })
        $(document).on('mouseup', function() {
          isDrag = false;
        });

        $(window).on('resize', function(){
            ps.update();
        });
    })
    
});



