ymaps.ready(init);

function init() {
	var myMap = new ymaps.Map("map", {
		center: [56.150404, 40.376361],
		zoom: 16
	});

	myPlacemark = new ymaps.Placemark([56.150404, 40.376361], {
		balloonContent: 'Владимир, пр-кт Строителей, 1А'
	}, {
		preset: 'islands#redIcon'
	});

	myMap.geoObjects
		.add(myPlacemark)
}