<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
?>

<?php 
	foreach ($arResult['ITEMS'] as &$item) {
		$item['IMAGE'] = CFile::ResizeImageGet(
			$item['PREVIEW_PICTURE']['ID'], 
			array(
				'width'=>27, 
				'height'=>27
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true
		);  
	}
?>
