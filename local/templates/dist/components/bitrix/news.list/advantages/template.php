<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) : ?>
	<div class="advantages">
		<div class="container">
			<div class="advantages__inner animated" data-animate="fadeUp">
				<? foreach($arResult['ITEMS'] as $item) : ?>
					<a class="advantages__item" href="#">
					<div class="advantages__logo">
						<div class="advantages__logo-icon">
							<img src="<?=$item['IMAGE']['src'] ?>">
						</div>
					</div>
					<div class="advantages__title"><?=$item['NAME'] ?></div>
					<div class="advantages__text"><?=$item['PREVIEW_TEXT'] ?></div>
					</a>
				<? endforeach; ?>
			</div>
		</div>
	</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">ADVANTAGES IS EMPTY </p>
	</div>
<? endif ?>