<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
?>

<?php 
	foreach ($arResult['ITEMS'] as &$item) {
		$item['RESIZE_PREVIEW_PICTURE'] = CFile::ResizeImageGet(
			$item['PREVIEW_PICTURE']['ID'], 
			array(
				'width'=>100, 
				'height'=>100
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true
		);		
	}
?>