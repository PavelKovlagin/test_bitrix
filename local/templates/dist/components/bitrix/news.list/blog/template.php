<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult['ITEMS']) : ?>
  <div class="blog" id="blog">
    <div class="blog__inner">
      <div class="block-caption animated" data-animate="fadeUp">
        <h2 class="block-caption__title">OUR BLOG</h2>
        <h3 class="block-caption__subtitle">Suspendisse sed eros mollis, tincidunt felis eget, interdum eratullam sit amet odio.</h3>
        <div class="dividing-line"></div>
      </div>
      <div class="blog__content animated" data-animate="fadeUp">
  <? foreach($arResult['ITEMS'] as $item) : ?>
        <div class="blog__item">
          <div class="item__img" style="background-image: url('<?= $item['RESIZE_PREVIEW_PICTURE']['src'] ?>');"></div>
          <div class="item__content">
            <div class="content__supTitle"><?= $item['PROPERTIES']['SUP_TITLE']['VALUE'] ?></div>
            <div class="content__title"><?= $item['NAME'] ?></div>
            <div class="content__text"><?= $item['PREVIEW_TEXT'] ?></div>
    <a class="content__footerText" href="<?= $item['PROPERTIES']['BUTTON_LINK']['VALUE'] ?>"><?= $item['PROPERTIES']['BUTTON_TEXT']['VALUE'] ?></a>
          </div>
        </div>
  <? endforeach ?>

      </div>
    </div>
  </div>
<? else : ?>
<div align="center">
  <p style="color: red; font-size: 300%">BLOG IS EMPTY </p>
</div>
<? endif ?>