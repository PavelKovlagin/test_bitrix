<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
  <div class="feedback" id="contact">
    <div class="container">
      <div class="block-caption">
        <h2 class="block-caption__title"><?= $item['NAME'] ?></h2>
        <h3 class="block-caption__subtitle"><?= $item['PREVIEW_TEXT'] ?></h3>
        <div class="dividing-line"></div>
      </div>
      <div class="feedback__inner animated" data-animate="fadeUp">
        <div class="contacts">
          <div class="contacts__inner">
            <div class="contacts__address">
              <div class="contacts__title">OUR ADDRESS</div>
              <div class="contacts__text">
              <? for($key = 0; $key < count($item['PROPERTIES']['ADDRESS']['VALUE']); $key++) : ?>
                <p><?= $item['PROPERTIES']['ADDRESS']['VALUE'][$key] ?></p>
              <? endfor ?>
              </div>
            </div>
            <div class="contacts__phone">
              <div class="contacts__title">CALL US</div>
              <div class="contacts__text">
              <? for($key = 0; $key < count($item['PROPERTIES']['TELEPHONE']['VALUE']); $key++) : ?>
                <p><?= $item['PROPERTIES']['TELEPHONE']['VALUE'][$key] ?></p>
              <? endfor ?>
              </div>
            </div>
            <div class="contacts__email">
              <div class="contacts__title">EMAIL US</div>
              <div class="contacts__text">
              <? for($key = 0; $key < count($item['PROPERTIES']['EMAIL']['VALUE']); $key++) : ?>
                <a href="mailto:<?= $item['PROPERTIES']['EMAIL']['VALUE'][$key] ?>"><p><span><?= $item['PROPERTIES']['EMAIL']['VALUE'][$key] ?></span></p></a>
              <? endfor ?>
              </div>
            </div>
          </div>
        </div>
        <div class="feedback__form">
          <form class="form" action="#" data-validate="data-validate">
            <div class="select">
              <div class="select__title">Name</div>
              <input class="form__input form__input--name" type="text" name="name" autocomplete="off" required="required"/>
            </div>
            <div class="select">
              <div class="select__title">Email</div>
              <input class="form__input form__input--email" type="email" name="email" autocomplete="off" required="required"/>
            </div>
            <div class="select">
              <div class="select__title">Subject</div>
              <input class="form__input form__input--subject" type="text" name="subject" autocomplete="off" required="required"/>
            </div>
            <div class="select">
              <div class="select__title">Message</div>
              <textarea class="form__message" name="message"></textarea>
              <button class="btn form__btn" type="submit">send message</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">FEEDBACK IS EMPTY </p>
	</div>
<? endif ?>