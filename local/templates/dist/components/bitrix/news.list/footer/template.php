<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
	<div class="footer" id="footer">
		<div class="container">
			<div class="footer__inner">
			<div class="footer__social">
				<a href="<?= $item['PROPERTIES']['FACEBOOK_LINK']['VALUE'] ?>"><i class="fab fa-facebook-f"></i></a>
				<a href="<?= $item['PROPERTIES']['TWITTER_LINK']['VALUE'] ?>"><i class="fab fa-twitter"></i></a>
				<a href="<?= $item['PROPERTIES']['RSS_LINK']['VALUE'] ?>"><i class="fas fa-rss"></i></a>
				<a href="<?= $item['PROPERTIES']['GOOGLE_LINK']['VALUE'] ?>"><i class="fab fa-google"></i></a>
				<a href="<?= $item['PROPERTIES']['LINKEDLN_LINK']['VALUE'] ?>"><i class="fab fa-linkedin-in"></i></a>
				<a href="<?= $item['PROPERTIES']['SKYPE_LINK']['VALUE'] ?>"><i class="fab fa-skype"></i></a>
				<a href="<?= $item['PROPERTIES']['VIMEO_LINK']['VALUE'] ?>"><i class="fab fa-vimeo-v"></i></a>
				<a href="<?= $item['PROPERTIES']['TUMBLR_LINK']['VALUE'] ?>"><i class="fab fa-tumblr"></i></a>
				</div><a class="footer__author"><?= $item['PREVIEW_TEXT'] ?> </a>
			</div>
		</div><a class="btn__footer" href="#" data-scroll="#intro"></a>
	</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">FOOTER IS EMPTY </p>
	</div>
<? endif ?>