<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
?> 

<?php 
	$item = &$arResult['ITEMS'][0];
	if ($item) {
		$item['IMAGE'] = CFile::ResizeImageGet(
			$item['PREVIEW_PICTURE']['ID'], 
			array(
				'width'=>350, 
				'height'=>350
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true
		); 
	}	 
?>
