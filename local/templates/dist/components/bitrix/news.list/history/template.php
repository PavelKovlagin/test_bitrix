<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
	<div class="history" id="about">
		<div class="container">
			<div class="history__inner animated" data-animate="fadeUp">
				<div class="history__item history__item--img">
					<div class="history__img">
						<div class="history__bg-img" style="background-image: url(<?=$item['IMAGE']['src'] ?>)"></div>
						<div class="history__btn"><a class="btn" href="<?=$item['PROPERTIES']['LINK']['VALUE'] ?>"><?=$item['PROPERTIES']['TEXT_BUTTON']['VALUE'] ?></a></div>
					</div>
				</div>
				<div class="history__item history__item--content">
					<div class="history__content">
						<div class="history__title"><?=$item['NAME'] ?></div>
						<div class="history__text">
							<?=$item["PREVIEW_TEXT"]; ?>
							<a class="btn history__btn" href="<?=$item['PROPERTIES']['LINK']['VALUE'] ?>"><?=$item['PROPERTIES']['TEXT_BUTTON']['VALUE'] ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">HISTORY IS EMPTY </p>
	</div>
<? endif ?>