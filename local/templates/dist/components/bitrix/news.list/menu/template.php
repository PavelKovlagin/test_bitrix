<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
	<svg style="display:none">
	<symbol id="anchor" viewBox="0 0 22 31">
		<path class="cls-1" data-name="anchor" d="M970.89,988.3l-2.509-3.774a0.635,0.635,0,0,0-1.062,0L964.81,988.3a0.667,0.667,0,0,0-.036.674,0.64,0.64,0,0,0,.567.348h1.238a6.853,6.853,0,0,1-5.083,4.613V979.466h2a1.018,1.018,0,0,0,0-2.036h-1.028a3.726,3.726,0,0,0,1.176-2.717,3.64,3.64,0,1,0-7.279,0,3.73,3.73,0,0,0,1.176,2.717h-1.027a1.018,1.018,0,0,0,0,2.036h2v14.465a6.852,6.852,0,0,1-5.084-4.613h1.239a0.642,0.642,0,0,0,.567-0.348,0.665,0.665,0,0,0-.037-0.674l-2.509-3.774a0.634,0.634,0,0,0-1.061,0L949.11,988.3a0.665,0.665,0,0,0-.036.674,0.64,0.64,0,0,0,.567.348h1.349a11.077,11.077,0,0,0,6.78,8.238l-0.249.312a0.664,0.664,0,0,0-.038.774l1.981,3.058a0.628,0.628,0,0,0,1.071,0l1.981-3.058a0.663,0.663,0,0,0-.038-0.774l-0.248-.312a11.075,11.075,0,0,0,6.779-8.238h1.35a0.64,0.64,0,0,0,.567-0.348A0.667,0.667,0,0,0,970.89,988.3ZM960,973.034a1.679,1.679,0,1,1-1.645,1.679A1.664,1.664,0,0,1,960,973.034Z" transform="translate(-949 -971)"></path>
	</symbol>
	</svg>
	<div  class="header" id="header">
	<div class="container">
		<div class="header__inner"><a class="header__logo" href="#" data-scroll="#intro"><img src="<?=$item['PREVIEW_PICTURE']['SRC'] ?>" alt="logo"/></a>
		<div class="header__nav" id="nav">
			<div class="header__nav-inner">
				<? for($key = 0; $key < count($item['PROPERTIES']['MENU_TEXT']['VALUE']); $key++) : ?>	
					<a class="header__nav-link" href="#" data-scroll="<?=$item['PROPERTIES']['MENU_LINK']['VALUE'][$key] ?>"><?=$item['PROPERTIES']['MENU_TEXT']['VALUE'][$key] ?></a>
				<? endfor ?>
			</div>
		</div>
		<button class="nav-toggle" id="nav_toggle" type="button"><span class="nav-toggle__item"></span></button>
		</div>
	</div>
	</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">MENU IS EMPTY </p>
	</div>
<? endif ?>