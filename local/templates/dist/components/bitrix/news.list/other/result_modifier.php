<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
?>

<?php 
	$item = &$arResult['ITEMS'][0];
	if ($item) {
		for ($key = 0; $key < count($item['PROPERTIES']['OTHER_IMAGE_1']['VALUE']); $key++) {
			$item['PROPERTIES']['RESIZE_OTHER_IMAGE_1'][$key] = CFile::ResizeImageGet(
				CFile::GetFileArray($item['PROPERTIES']['OTHER_IMAGE_1']['VALUE'][$key])['ID'], 
				array(
					'width'=>100, 
					'height'=>100
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL, 
				true
			);  
			$item['PROPERTIES']['RESIZE_OTHER_IMAGE_2'][$key] = CFile::ResizeImageGet(
				CFile::GetFileArray($item['PROPERTIES']['OTHER_IMAGE_2']['VALUE'][$key])['ID'], 
				array(
					'width'=>100, 
					'height'=>100
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL, 
				true
			);
		}
	}		
?>