<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>
<? if ($item) : ?>
  <div class="others">
    <div class="container">
      <div class="block-caption animated" data-animate="fadeUp">
        <h2 class="block-caption__title"><?=$item['NAME'] ?></h2>
        <h3 class="block-caption__subtitle"><?=$item['PREVIEW_TEXT'] ?></h3>
        <div class="dividing-line"></div>
      </div>
      <div class="others__content animated" data-animate="fadeUp">
        <? for($key = 0; $key < count($item['PROPERTIES']['OTHER_IMAGE_1']['VALUE']); $key++) : ?>
          <a class="others__item" href="#">
            <img src="<?= $item['PROPERTIES']['RESIZE_OTHER_IMAGE_1'][$key]['src'] ?>" class="others__img-1">
            <img src="<?= $item['PROPERTIES']['RESIZE_OTHER_IMAGE_2'][$key]['src'] ?>" class="others__img-2">
          </a>
        <? endfor ?>
        
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">OTHER IS EMPTY </p>
	</div>
<? endif ?>