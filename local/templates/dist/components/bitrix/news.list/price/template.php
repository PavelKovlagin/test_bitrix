<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
  <div class="price" id="pricing">
    <div class="container">
      <div class="block-caption animated" data-animate="fadeUp">
        <h2 class="block-caption__title"><?=$item['NAME'] ?></h2>
        <h3 class="block-caption__subtitle" data-aos="fade-left"><?=$item['PREVIEW_TEXT'] ?></h3>
        <div class="dividing-line"></div>
      </div>
      <div class="price__content animated" data-animate="fadeUp">
        <? for ($key = 0; $key < count($item['PROPERTIES']['PRICE_ITEM_HEADER']['VALUE']); $key++) : ?>
          <div class="price-item">
            <div class="price-item__header"><?=$item['PROPERTIES']['PRICE_ITEM_HEADER']['VALUE'][$key] ?></div>
            <div class="price-circle">
              <div class="price-circle__external">
                <div class="price-circle__cost"><?=$item['PROPERTIES']['PRICE_CIRCLE_COST']['VALUE'][$key] ?></div>
                <div class="price-circle__text"><?=$item['PROPERTIES']['PRICE_CIRCLE_TEXT']['VALUE'][$key] ?></div>
              </div>
            </div>
            <div class="price-item__info">
              <div class="price-item__text"><?=$item['PROPERTIES']['PRICE_ITEM_TEXT']['VALUE'][$key] ?></div>
              <a class="btn price-item__btn" onclick="location.href='<?=$item['PROPERTIES']['PRICE_ITEM_BUTTON_LINK']['VALUE'][$key] ?>'" ><?=$item['PROPERTIES']['PRICE_ITEM_BUTTON_TEXT']['VALUE'][$key] ?></a>
            </div>
          </div>
        <? endfor ?>
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">PRICE IS EMPTY </p>
	</div>
<? endif ?>