<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) : ?>
	<div class="service" data-slider="data-slider">
		<div class="service-slider" data-service-slider="data-service-slider">

			<? foreach($arResult['ITEMS'] as $item) : ?>
				<div class="service-slider-item">
					<div class="service__inner animated" data-animate="fadeUp">
					<div class="service-content">
						<div class="service-content__title" data-slider="data-slider">
						<div class="service-content__title-text"><?= $item['NAME'] ?></div>
						</div>
							<div class="service-content__inner">
								<? for($key = 0; $key < count($item['PROPERTIES']['CAPTION_TITLE']['VALUE']); $key++ ) : ?>
									<div class="service-content__caption"><p><?= $item['PROPERTIES']['CAPTION_TITLE']['VALUE'][$key]; ?> </p><a class="service-icon" href="#"> 
									<div class="service-icon__inner"><img src="<?= $item['PROPERTIES']['RESIZE_CAPTION_IMAGE'][$key]['src']; ?>" class="service-icon__logo" alt=""></div></a></div>
									<div class="service-content__text"><?=$item['PROPERTIES']['CAPTION_TEXT']['VALUE'][$key]; ?></div>
								<? endfor ?>
						</div>
					</div>
					<div class="service-gallery">
						<div class="service-gallery__img" style="background-image: url(<?= $item['RESIZE_PREVIEW_PICTURE']['src'] ?>)"></div>
					</div>
					</div>
				</div>
			<? endforeach; ?>          
			
		</div>
	</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">SERVICES IS EMPTY </p>
	</div>
<? endif ?>