<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) : ?>

<div class="page" id="intro">
	<div class="page-slider" data-slider="data-slider">
		<? foreach($arResult['ITEMS'] as $item) : ?>
		<div class="page-slider__item" style="background-image: url('<?=$item['IMAGE']['src']?>')">
			<div class="container">
				<div class="page-slider__inner">
				<div class="page-caption">
					<div class="page-caption__suptitle"><?=$item['PROPERTIES']['UPPER_TEXT']['VALUE']?></div>
						<h1 class="page-caption__title"><?=$item['NAME']?></h1>
						<div class="page-caption__dividing-line dividing-line"></div>
						<div class="page-caption__text"><?=$item['PREVIEW_TEXT']?></div>
						<div class="page-caption__buttons">
							<a href="<?= $item['PROPERTIES']['LINK_1']['VALUE']?>">
								<button class="btn page-caption__btn page-caption__btn--left">
									<?= $item['PROPERTIES']['TEXT_BUTTON_1']['VALUE']?>
								</button>
							</a>
							<a href="<?= $item['PROPERTIES']['LINK_2']['VALUE']?>"> <button class="btn page-caption__btn page-caption__btn--right"><?=$item['PROPERTIES']['TEXT_BUTTON_2']['VALUE']?></button> </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<? endforeach; ?>
	</div>
<a class="page-anchor animated" href="#" data-scroll="#footer" data-animate="fadeUp"></a>
</div>

<? else : ?>

	<div align="center">
		<p style="color: red; font-size: 300%">SLYDER IS EMPTY </p>
	</div>

<? endif ?>