<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
  <div class="stats animated" data-animate="fadeUp">
    <div class="stats__bg" style="background-image: url('<?=  $item['RESIZE_PREVIEW_PICTURE']['src'] ?>');">
      <div class="container">
        <div class="stats__inner">
          <? for($key = 0; $key < count($item['PROPERTIES']['STATS_LOGO']['VALUE']); $key++) : ?>
            <div class="stats__item">
              <div class="stats__logo"><img src="<?= $item['PROPERTIES']['RESIZE_STATS_LOGO'][$key]['src'] ?>" class="stats__icon" alt=""></div>
              <div class="stats__number"><?=$item['PROPERTIES']['STATS_NUMBER']['VALUE'][$key] ?></div>
              <div class="stats__text"><?=$item['PROPERTIES']['STATS_TEXT']['VALUE'][$key] ?></div>
            </div>
          <? endfor ?>
        </div>
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">STATS IS EMPTY </p>
	</div>
<? endif ?>