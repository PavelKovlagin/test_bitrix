<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
  <div class="study">
    <div class="container">
      <div class="study__inner animated" data-animate="fadeUp">
        <div class="block-caption">
          <h2 class="block-caption__title"><?=$item['NAME'] ?></h2>
          <h3 class="block-caption__subtitle" data-aos="fade-left"><?=$item['PREVIEW_TEXT'] ?></h3>
          <div class="dividing-line"></div>
        </div>
        <div class="study-content">
          <div class="study-slider" data-study-slider="data-study-slider">
            <? for($key = 0; $key < count($item['PROPERTIES']['SLIDER_TITLE']['VALUE']); $key++) : ?>
                <div class="study-slider__item">
                <div class="study-slider__content">
                    <div class="study-slider__content-inner">
                    <div class="study-slider__logo">
                        <div class="study-slider__bg"></div>
                        <div class="study-slider__icon" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_SLIDER_ICON'][$key]['src'] ?>');"></div>
                    </div>
                      <div class="study-slider__title"><?=$item['PROPERTIES']['SLIDER_TITLE']['VALUE'][$key] ?></div>
                      <div class="study-slider__text"><?=$item['PROPERTIES']['SLIDER_TEXT']['VALUE'][$key] ?></div>
                      <a class="btn study-slider__btn" href="<?=$item['PROPERTIES']['BUTTON_LINK']['VALUE'][$key] ?>"><?=$item['PROPERTIES']['BUTTON_TEXT']['VALUE'][$key] ?></a>
                    </div>
                </div>
                <div class="study-slider__img-content">
                    <div class="study-slider__img" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_SLIDER_IMAGE'][$key]['src'] ?>');"></div>
                </div>
                </div>
            <? endfor ?>                
          </div>
        </div>
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">STUDY IS EMPTY </p>
	</div>
<? endif ?>