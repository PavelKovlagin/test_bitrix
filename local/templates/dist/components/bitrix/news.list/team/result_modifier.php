<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
?>

<?php 
	foreach ($arResult['ITEMS'] as &$item) {
		$item['PROPERTIES']['RESIZE_BACKGROUND'] = CFile::ResizeImageGet(
			CFile::GetFileArray($item['PROPERTIES']['BACKGROUND']['VALUE'])['ID'], 
			array(
				'width'=>200, 
				'height'=>200
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true
		);		
		$item['PROPERTIES']['RESIZE_BACKGROUND_RED'] = CFile::ResizeImageGet(
			CFile::GetFileArray($item['PROPERTIES']['BACKGROUND_RED']['VALUE'])['ID'],
			array(
				'width'=>200, 
				'height'=>200
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true
		);
		for ($key = 0; $key < count($item['PROPERTIES']['TEAM_NAME']['VALUE']); $key++) {
			$item['PROPERTIES']['RESIZE_TEAM_IMAGE'][$key] = CFile::ResizeImageGet(
				CFile::GetFileArray($item['PROPERTIES']['TEAM_IMAGE']['VALUE'][$key])['ID'], 
				array(
					'width'=>100, 
					'height'=>100
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL, 
				true
			);  
		}
	}
?>

