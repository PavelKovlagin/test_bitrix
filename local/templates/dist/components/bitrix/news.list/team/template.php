<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) : ?>
  <div class="team" id="team">
    <div class="block-caption blog-caption--team animated" data-animate="fadeUp">
      <h2 class="block-caption__title">our team</h2>
      <h3 class="block-caption__subtitle">Meet the craziest team. Share your thoughts with them.</h3>
      <div class="dividing-line"></div>
    </div>
    <div class="team-slider animated" data-team-slider="data-team-slider" data-animate="fadeUp"> 
      <? foreach($arResult['ITEMS'] as $item) : ?>
      <div class="team-slider__item">
        <div class="team__bg">
          <div class="team__bg-inner" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_BACKGROUND']['src'] ?>');"></div>
          <div class="team__bg-red" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_BACKGROUND_RED']['src'] ?>');"></div>
        </div>
        <div class="team__members">    
          <div class="container">
            <div class="team__inner"><a class="team__member" href="<?=$item['PROPERTIES']['TEAM_LINK']['VALUE'][0] ?>">
            <? $count = count($item['PROPERTIES']['TEAM_NAME']['VALUE']) ?>
            <? for($key = 0; $key < $count-1; $key++) : ?>
                <div class="team__circle">
                  <div class="team__img" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_TEAM_IMAGE'][$key]['src'] ?>');"></div>
                </div>
                <div class="team__info">
                  <div class="info__name"><?=$item['PROPERTIES']['TEAM_NAME']['VALUE'][$key] ?></div>
                  <div class="info__prof"><?=$item['PROPERTIES']['TEAM_PROF']['VALUE'][$key] ?></div>
                </div></a><a class="team__member" href="<?=$item['PROPERTIES']['TEAM_LINK']['VALUE'][$key] ?>">
              <? endfor ?>
                
                
              <div class="team__circle">
                <div class="team__img" style="background-image: url('<?= $item['PROPERTIES']['RESIZE_TEAM_IMAGE'][$count-1]['src'] ?>');"></div>
              </div>
              <div class="team__info">
              <div class="info__name"><?=$item['PROPERTIES']['TEAM_NAME']['VALUE'][$count-1] ?></div>
                  <div class="info__prof"><?=$item['PROPERTIES']['TEAM_PROF']['VALUE'][$count-1] ?></div>
              </div></a></div>
          </div>
        </div>  
      </div>

      <? endforeach ?>    
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">TEAM IS EMPTY </p>
	</div>
<? endif ?>