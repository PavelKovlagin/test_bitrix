<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>

<? if ($item) : ?>
  <div class="trail">
    <div class="trail__bg" style="background-image: url('<?=$item['RESIZE_PREVIEW_PICTURE']['src'] ?>');">
      <div class="container">
        <div class="trail__inner">
          <div class="block-caption">
            <h2 class="block-caption__title"><p><?=$item['PREVIEW_TEXT'] ?></p></h2>
              <p></p>
            
            <h3 class="block-caption__subtitle"><?=$item['DETAIL_TEXT'] ?></h3>
          </div><a class="btn" href="<?=$item['PROPERTIES']['BUTTON_LINK']['VALUE'] ?>"><?=$item['PROPERTIES']['BUTTON_TEXT']['VALUE'] ?></a>
        </div>
      </div>
    </div>
  </div>
<? else : ?>
  <div align="center">
		<p style="color: red; font-size: 300%">TRAIL IS EMPTY </p>
	</div>
<? endif ?>