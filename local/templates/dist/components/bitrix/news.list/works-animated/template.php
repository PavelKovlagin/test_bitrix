<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) : ?>
    <div class="works-card animated" data-animate="fadeUp">
    <? foreach($arResult['ITEMS'] as $item) : ?>
        <div class="works-card__item">
        <div class="works-card__inner">
            <div class="works-card__img" style="background-image: url('<?=$item['RESIZE_PREVIEW_PICTURE']['src'] ?>');"></div>
            <div class="works-card__info">
                <div class="works-card__title"><?=$item['NAME'] ?></div>
                <div class="works-card__text"><?=$item['PREVIEW_TEXT'] ?></div>
            </div>
        </div>
        </div>
    <? endforeach ?>
<? else : ?>
    <div align="center">
		<p style="color: red; font-size: 300%">WORKS CARD IS EMPTY </p>
	</div>
<? endif ?>
