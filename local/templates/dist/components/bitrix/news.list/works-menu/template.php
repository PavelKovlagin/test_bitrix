<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$item = $arResult['ITEMS'][0];
?>
<? if ($item) : ?>
    <div class="block-caption">
        <h2 class="block-caption__title"><?=$item['NAME']?></h2>
        <h3 class="block-caption__subtitle" data-aos="fade-left"><?=$item['PREVIEW_TEXT']?></h3>
        <div class="dividing-line"></div>
    </div>
    <div class="works-filter" data-aos="fade-left">

        <? for($key = 0; $key < count($item['PROPERTIES']['WORKS_FILTER']['VALUE']); $key++) : ?>
            <button class="works-filter__item" type="button"><?=$item['PROPERTIES']['WORKS_FILTER']['VALUE'][$key]?></button>
        <? endfor; ?>
    </div>
    <div class="works-category"><a href="" id="show">Category</a></div>
    <div class="works-overlay"></div>
    <select class="works-overlay__select" name="select">
        <? for($key = 0; $key < count($item['PROPERTIES']['WORKS_FILTER']['VALUE']); $key++) : ?>
            <option value="<?=$key?>"><?=$item['PROPERTIES']['WORKS_FILTER']['VALUE'][$key]?></option>
        <? endfor; ?>
    </select>
<? else : ?>
    <div align="center">
		<p style="color: red; font-size: 300%">WORKS IS EMPTY </p>
	</div>
<? endif ?>