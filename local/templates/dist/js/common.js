$(function(){

  var header = $("#header"),
        headerHeight = $('.header').outerHeight();
        introH = $("#intro").innerHeight(),
        scrolledPrev = $(window).scrollTop();
        windowHeight = $(window).height();

  // Nav Toggle 
  var nav = $("#nav");
  var navToggle = $("#nav_toggle");

  // скрипт для работы шапки при скролле
  $(window).on('scroll', function(event) {
    $('.nav-toggle').removeClass('active');
    
    if($(window).width() <= '1259'){
      nav.slideUp(400);
    }

    const scrolled = $(this).scrollTop();
    // nav.slideUp(400);

    if(scrolled >= scrolledPrev) {
      // вниз
      if(header.hasClass('fixed')) {

          setTimeout(function() {
              header.removeClass('fixed');
          }, 400);
      }
    } else {
      // вверх
        if(!header.hasClass('fixed')) {
            header.addClass('hidden noanim fixed');
            
            setTimeout(function() {
                header.removeClass('noanim');
            }, 0);
            
            setTimeout(function() {
                header.removeClass('hidden');
            }, 1);
        }

        if(scrolled <= 0) {
            header.removeClass('fixed');
        }
      }

    scrolledPrev = scrolled;
  });

  // скрипт для бургера
  navToggle.on("click", function(event) {
    event.preventDefault();

    nav.slideToggle(400);

    // Главная строка :)
    $('.nav-toggle').toggleClass('active');
  });

  // скрипт на сокрытие меню шапки на мобилках
  $(document).mouseup(function (e){ 
      var block = $(".nav-toggle"); 
      if (!block.is(e.target) 
        && block.has(e.target).length === 0 && ($(window).width() <= '1259')) {
        $('.nav-toggle').removeClass('active');
        nav.slideUp(400); 
      }
  });

  // скрипт для кооректной работы шапки при resize
  $(window).on('resize', function(event) {
    if($(window).width() > '1259'){
      nav.css('display', 'flex');
    }else{
      nav.css('display', 'none');
      $('.nav-toggle').removeClass('active');
    }
  });

  // скрипт для подсчёта высоты шапки на мобилках
  $(window).on('resize', function(event) {
    if($(window).width() < '768'){
      var windowHeight = $(window).height();  
      $('.header__nav-inner').css('height', windowHeight);
    }else{
      $('.header__nav-inner').css('height', 'auto');
    }
  });

  if($(window).width() < '768'){
    var windowHeight = $(window).height();  
    $('.header__nav-inner').css('height', windowHeight);
  };

  // скрипт на скроллы к элементам
  $("[data-scroll]").on("click", function(event) {
    event.preventDefault();

    var $this = $(this),
        blockId = $this.data('scroll'),
        blockOffset = $(blockId).offset().top;

    $("#nav a").removeClass("active");
    $this.addClass("active");

    $("html, body").animate({
        scrollTop: blockOffset
    }, 1000);

    $("#nav").toggleClass("active");
    $("#nav_toggle").toggleClass("active")
    
  });
  
  // page-slider
  $(function(){
    // slider
    $("[data-slider]").slick({
      infinite: true,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      autoplay: true,
      autoplaySpeed: 3000,
    });
  });

  // service-slider
  $(function(){
    // slider
    $("[data-service-slider]").slick({
      infinite: true,
      fade:true,
      dots:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      // autoplay: true,
      autoplaySpeed: 3000,
      verticalSwiping: false,
      arrows:false
    });
  });

  // study-slider
  $(function(){
    // slider
    $("[data-study-slider]").slick({
      infinite: true,
      fade:true,
      dots:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows:false
    });
  });

  // team-slider
  $(function(){
    // slider
    $("[data-team-slider]").slick({
      infinite: true,
      fade:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows:false
    });
  });

  // скрипт на выбранный прайс
  $('.price-item').on('click', function(event){
    event.preventDefault();
    if(!$(this).children('.price-item__header').hasClass('price-item__header--is-active')){
      $('.price-item__header').removeClass('price-item__header--is-active');
      $(this).children('.price-item__header').addClass('price-item__header--is-active');
    }
  });

  // скрипт на заголовок input в форме
  $('.form__input').on('focus', function() {
    $(this).parents('.select').children('.select__title').addClass('select__title--active');
  });

  $('.form__input').on('blur', function() {
    if($(this).val().length < 1){
      $(this).parents('.select').children('.select__title').removeClass('select__title--active');
    }
  });

  // скрипт для селекта в works
  $('select').styler({
    selectSearchLimit: 30
  });

  $('#show').click(function(e) {
    e.preventDefault();
    $('.works-overlay__select, .works-overlay').show();
  })

  $('.works-overlay').click(function(e) {
    $('.works-overlay__select, .works-overlay').hide();
  })

  // валидация формы
  $.extend( $.validator.messages, {
    required: "Это поле необходимо заполнить.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный email.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
  } );
  
  $('.form').validate({
      errorPlacement: function(error, element) {
          var $parent = element.parent();
          $parent.append(error);
      },
      submitHandler: function(form) {
              $(form).trigger("formSubmit");
      }
  
  });
  
  $('.form__input--name').on('input', function() {
    var value = $(this).val();
    value = value.replace(/[^a-zA-Zа-яА-ЯЁё\s\-]/ig, '');
    $(this).val(value);
  });

  // всплывающая анимашка
  $(document).on('scroll', animateScroll);

  function animateScroll() {
    $('[data-animate]').each(function () {
      var delta = 0.8;
  
      if ($(this).is('[data-animate-nooffset]')) {
        delta = 1;
      }
  
      var scrolled = $(document).scrollTop() + window.innerHeight * delta;
      var offset = $(this).offset().top;
      if (offset <= scrolled) {
        var cl = $(this).attr('data-animate');
        $(this).addClass(cl).removeAttr('data-animate');
      }
    });
  }

});
