<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/

for ($key = 0; $key < count($arResult['PROPERTIES']['IMAGE']['VALUE']); $key++) {
    $arResult['PROPERTIES']['RESIZE_IMAGE'][$key] = CFile::ResizeImageGet(
        CFile::GetFileArray($arResult['PROPERTIES']['IMAGE']['VALUE'][$key])['ID'], 
        array(
            'width'=>100, 
            'height'=>100
        ), 
        BX_RESIZE_IMAGE_PROPORTIONAL, 
        true
    );  
}
