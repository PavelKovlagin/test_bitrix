<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="main">
	<div class="container">
		<div class="main__inner">
			<h1 class="h1"><?= $arResult['PREVIEW_TEXT'] ?></h1>
			<p class="main__text text"><?= $arResult['DETAIL_TEXT'] ?></p>
			<div class="main__arrows">
				<svg xmlns="http://www.w3.org/2000/svg" width="196" height="42" viewBox="0 0 196 42" fill="none">
					<path d="M13 0V41M13 41L25 29.1446M13 41L1 29.1446M47 0V41M47 41L59 29.1446M47 41L35 29.1446M81 0V41M81 41L93 29.1446M81 41L69 29.1446M115 0V41M115 41L127 29.1446M115 41L103 29.1446M149 0V41M149 41L161 29.1446M149 41L137 29.1446M183 0V41M183 41L195 29.1446M183 41L171 29.1446" stroke="#333333"/>
				</svg>
			</div>
			<div class="main__previews">
				<? for($key = 0; $key < count($arResult['PROPERTIES']['RESIZE_IMAGE']); $key++) : ?>
					<div class="main__previews-item" data-src="<?= $arResult['PROPERTIES']['RESIZE_IMAGE'][$key]['src'] ?>"></div>
				<? endfor ?>
			</div>
		</div>
	</div>
</div>