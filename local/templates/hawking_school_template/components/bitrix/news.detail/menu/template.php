<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="header">
	<div class="container">
		<div class="header__inner"><a class="logo js-anchor" href="#"><img src="<?= $arResult['PROPERTIES']['LOGO'] ?>" alt="logo" /></a>
			<div class="burger js-burger">
				<div></div>
				<div></div>
				<div></div>
			</div>
			<div class="menu js-menu">
				<div class="menu__inner">
					<? for($key = 0; $key < count($arResult['PROPERTIES']['NAME']['VALUE']); $key++) : ?>
						<div class="menu__item"><a class="menu__link js-anchor" href="<?= $arResult['PROPERTIES']['LINK']['VALUE'][$key] ?>"><?= $arResult['PROPERTIES']['NAME']['VALUE'][$key] ?></a></div>
					<? endfor ?>
				</div>
			</div>
		</div>
	</div>
</div>