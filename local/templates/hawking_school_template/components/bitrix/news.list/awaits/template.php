<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_awaits">
	<div class="awaits">
		<div class="container">
			<div class="awaits__inner">
				<h2 class="h2"><?= $arResult['NAME'] ?></h2>
				<div class="awaits__carousel swiper-container">
					<div class="awaits__list swiper-wrapper">
						<? for ($key = 0; $key < count($arResult['ITEMS']); $key = $key + 2) : ?>
						
							<div class="swiper-slide awaits__slide">
								<div class="awaits__item">
									<div class="awaits__item-pic">
										<img src="" data-src="<?= CFile::GetFileArray($arResult['ITEMS'][$key]['PROPERTIES']['ICON']['VALUE'])['SRC'] ?>" alt="">
									</div>
									<span class="awaits__item-text"><?= $arResult['ITEMS'][$key]['PREVIEW_TEXT'] ?></span>
								</div>
								<div class="awaits__item">
									<div class="awaits__item-pic">
										<img src="" data-src="<?= CFile::GetFileArray($arResult['ITEMS'][$key+1]['PROPERTIES']['ICON']['VALUE'])['SRC'] ?>" alt="">
									</div>
									<span class="awaits__item-text"><?= $arResult['ITEMS'][$key+1]['PREVIEW_TEXT'] ?></span>
								</div>
							</div>						
						<? endfor ?>				
					</div>
					<div class="awaits__carousel-bullets">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">AWAITS IS EMPTY </p>
	</div>
<? endif ?>