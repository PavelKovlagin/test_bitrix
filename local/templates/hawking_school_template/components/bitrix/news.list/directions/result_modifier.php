<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/

$newArr['NAME'] = $arResult['NAME'];
$arFilter = Array('IBLOCK_ID'=>$arResult['ID']);
$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
$db_list->NavStart(20);
while($ar_result = $db_list->GetNext())
{   
    $newArr['SECTIONS'][$ar_result['ID']]['NAME'] = $ar_result['NAME'];
    $newArr['SECTIONS'][$ar_result['ID']]['DESCRIPTION'] = $ar_result['DESCRIPTION'];
}

foreach ($arResult['ITEMS'] as &$item) {
    if ($item['IBLOCK_SECTION_ID']) {        
        for ($key = 0; $key < count($item['PROPERTIES']['IMAGE_INSTRUMENTS']['VALUE']); $key++) {
            $item['PROPERTIES']['RESIZE_IMAGE_INSTRUMENTS'][$key] = CFile::ResizeImageGet(
                CFile::GetFileArray($item['PROPERTIES']['IMAGE_INSTRUMENTS']['VALUE'][$key])['ID'], 
                array(
                    'width'=>70, 
                    'height'=>70
                ), 
                BX_RESIZE_IMAGE_PROPORTIONAL, 
                true
            ); 
        }
        $newArr['SECTIONS'][$item['IBLOCK_SECTION_ID']]['ITEMS'][] = $item;
    }
}

$arResult = $newArr;


