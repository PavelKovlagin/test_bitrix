<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<? if ($arResult) : ?>
<div class="section section_directions" id="directions">
	<div class="directions">
		<div class="container">
			<h2 class="h2"><?=$arResult['NAME'] ?></h2>
		</div>
		<? foreach ($arResult['SECTIONS'] as $section) : ?>
		<div class="directions__line js-directions-line" data-direction="rtl">
			<span> <?= $section['NAME'] ?> ▼ </span>
		</div>
		<div class="container">
			<div class="directions__main">
			<?= $section['DESCRIPTION'] ?>	
			<div class="directions__carousel swiper-container">
					<ul class="swiper-wrapper">

						<? foreach ($section['ITEMS'] as $item) : ?>

						<li class="swiper-slide direction__card card">
							<div class="card__head">
								<span class="card__pos"><?= $item['NAME'] ?></span>
								<span class="card__title"><?= $item['PREVIEW_TEXT'] ?></span></div>
							<div class="card__main">
								<div class="card__skills"><span class="card__subtitle">Компетенции:</span>
									<ol class="card__list list">										
										<? for ($key = 0; $key < count($item['PROPERTIES']['COMPETENCES']['VALUE']); $key++) : ?>
											<li class="list__item"><?= $item['PROPERTIES']['COMPETENCES']['VALUE'][$key] ?></li>
										<? endfor ?>
									</ol>
								</div>
								<div class="card__tools"><span class="card__subtitle">Инструменты:</span>
									<div class="card__logos">
										<? for ($key = 0; $key < count($item['PROPERTIES']['RESIZE_IMAGE_INSTRUMENTS']); $key++) : ?>
											<div class="card__logos-item">
												<img src="" data-src="<?= $item['PROPERTIES']['RESIZE_IMAGE_INSTRUMENTS'][$key]['src'] ?>" />
											</div>
										<? endfor ?>

									</div>
								</div>
							</div>
						</li>

						<? endforeach ?>

					</ul>
					<div class="directions__carousel-controls">
						<div class="directions__carousel-arrow directions__carousel-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20" fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z" fill="black" /></svg></div>
						<div class="directions__carousel-arrow directions__carousel-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20" fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z" fill="black" /></svg></div>
					</div>
					<div class="directions__carousel-bullets"></div>
				</div>
			</div>
		</div>
		<? endforeach ?>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">DIRECTIONS IS EMPTY </p>
	</div>
<? endif ?>