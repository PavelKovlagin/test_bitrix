<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_faq" id="faq">
	<div class="container">
		<h2 class="h2"><?= $arResult['NAME'] ?></h2>
		<div class="faq">
			<? foreach ($arResult['ITEMS'] as $item) : ?>
				<div class="faq__item">
					<span class="faq__item-question"><?= $item['PROPERTIES']['QUESTION']['VALUE'] ?></span>
					<span class="faq__item-answer"><?= $item['PROPERTIES']['ANSWER']['VALUE'] ?></span>
				</div>
			<? endforeach ?>
		</div>
		<span class="faq-additional text"><?= $arResult['DESCRIPTION'] ?></span>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">FAQ IS EMPTY </p>
	</div>
<? endif ?>