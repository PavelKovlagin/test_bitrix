<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_feedback" id="feedback">
	<div class="container">
		<div class="section__inner">
			<h2 class="h2"><?= $arResult['NAME'] ?></h2>
			<div class="feedback">
				<div class="feedback__photos">
					<div class="feedback__photos-item js-feedback-photo is-active" data-id="0"
						data-src="<?= $arResult['ITEMS'][0]['RESIZE_IMAGE']['src'] ?>"></div>
					<? for ($key = 1; $key < count($arResult['ITEMS']); $key++) : ?>
						<div class="feedback__photos-item js-feedback-photo" data-id="<?= $key ?>"
							data-src="<?= $arResult['ITEMS'][$key]['RESIZE_IMAGE']['src'] ?>"></div>
					<? endfor ?>
				</div>
				<div class="feedback__main">
					<div class="feedback__thumbs">
						<div class="feedback__thumbs-item js-feedback-thumb is-active" data-id="0"
							data-src="<?= $arResult['ITEMS'][0]['RESIZE_IMAGE']['src'] ?>"></div>
							<? for ($key = 1; $key < count($arResult['ITEMS']); $key++) : ?>
								<div class="feedback__thumbs-item js-feedback-thumb" data-id="<?= $key ?>"
									data-src="<?= $arResult['ITEMS'][$key]['RESIZE_IMAGE']['src'] ?>"></div>
							<? endfor ?>
					</div>

					<div class="feedback__contents">
						<div class="feedback__content js-feedback-content is-active" data-id="0"><span
								class="feedback__content-name"><?= $arResult['ITEMS'][0]['NAME'] ?></span>
							<p class="feedback__content-text"><?= $arResult['ITEMS'][0]['PREVIEW_TEXT'] ?></p>
						</div>
						<? for ($key = 1; $key < count($arResult['ITEMS']); $key++) : ?>
							<div class="feedback__content js-feedback-content" data-id="<?= $key ?>"><span
									class="feedback__content-name"><?= $arResult['ITEMS'][$key]['NAME'] ?></span>
								<p class="feedback__content-text"><?= $arResult['ITEMS'][$key]['PREVIEW_TEXT'] ?></p>
							</div>
						<? endfor ?>						
					</div>

					<div class="feedback__controls">
						<div
							class="feedback__controls-arrow feedback__controls-arrow_prev js-feedback-arrow-prev is-disabled">
							<svg xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20"
								fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z"
									fill="black" /></svg></div>
						<div
							class="feedback__controls-arrow feedback__controls-arrow_next js-feedback-arrow-next">
							<svg xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20"
								fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z"
									fill="black" /></svg></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">FEEDBACK IS EMPTY </p>
	</div>
<? endif ?>