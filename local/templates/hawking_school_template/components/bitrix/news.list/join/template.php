<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_join">
	<div class="join">
		<div class="container">
			<div class="join__inner">
				<h2 class="h2"><?= $arResult['NAME'] ?></h2>
				<div class="join__carousel swiper-container">
					<div class="join__list swiper-wrapper">
						<? foreach($arResult['ITEMS'] as $item) : ?>
							<div class="swiper-slide join__photo" data-src="<?= $item['RESIZE_IMAGE']['src'] ?>"></div>
						<? endforeach ?>
					</div>
					<div class="join__carousel-controls">
						<div class="join__carousel-arrow join__carousel-arrow_prev"><svg
								xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20"
								fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z"
									fill="black" /></svg></div>
						<div class="join__carousel-arrow join__carousel-arrow_next"><svg
								xmlns="http://www.w3.org/2000/svg" width="120" height="20" viewBox="0 0 120 20"
								fill="none">
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M109.707 0L120 10L109.707 20L109.004 19.317L118.097 10.4829H0V9.51707H118.097L109.004 0.68296L109.707 0Z"
									fill="black" /></svg></div>
					</div>
					<div class="join__carousel-bullets"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">JOIN IS EMPTY </p>
	</div>
<? endif ?>