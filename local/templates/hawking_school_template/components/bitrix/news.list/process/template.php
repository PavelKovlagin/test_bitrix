<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_process" id="selection">
	<div class="process">
		<div class="container">
			<div class="process__inner">
				<? foreach ($arResult['ITEMS'] as $item) : ?>
					<div class="process__column">
						<h2 class="h2"><?= $item['NAME'] ?></h2>
						<ol class="list">
							<? foreach ($item['PROPERTIES']['LIST_ITEMS']['~VALUE'] as $list_item) : ?>
								<li class="list__item"><?= $list_item ?></li>
							<? endforeach ?>	
						</ol>
					</div>
				<? endforeach ?>
				<div class="process__star js-rotate-scroll" data-direction="rtl">
					<svg width="394" height="394" viewBox="0 0 394 394" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M226.455 125.252L315 36.7071L356.719 78.4264L269.499 165.646L268.646 166.5H269.853H393.5V225.602H268.954H267.747L268.601 226.455L356.319 314.174L314.6 355.893L226.455 267.748L225.602 266.895V268.102V393.5H166.5V269.853V268.646L165.646 269.499L78.8263 356.319L37.107 314.6L125.252 226.455L126.105 225.602H124.898H0.5L0.500003 166.5H124H125.207L124.354 165.646L36.7071 78L78.4264 36.2807L165.646 123.501L166.5 124.354V123.147V0.5H225.602V124.898V126.105L226.455 125.252Z" stroke="#EB5757" />
					</svg>
				</div>
			</div>
		</div>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">PROCESS IS EMPTY </p>
	</div>
<? endif ?>