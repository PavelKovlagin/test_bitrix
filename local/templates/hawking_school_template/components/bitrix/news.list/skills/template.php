<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult) : ?>
<div class="section section_skills" id="skills">
	<div class="skills">
		<div class="container">
			<div class="skills__inner">
				<h2 class="h2"><?= $arResult['NAME'] ?></h2>
				<? foreach ($arResult['ITEMS'] as $item) : ?>
					<div class="skills__column"><span class="skills__title"><?= $item['NAME'] ?></span>
						<div class="skills__pic"><svg xmlns="http://www.w3.org/2000/svg" width="212" height="46" viewBox="0 0 212 46" fill="none">
								<path d="M1 44V2L43 44V2L85 44V2L127 44V2L169 44V2L211 44" stroke="#EB5757" /></svg></div>
						<ol class="list">
							<? for ($key = 0; $key < count($item['PROPERTIES']['SKILLS']['VALUE']); $key++) : ?>
								<li class="list__item"><?= $item['PROPERTIES']['SKILLS']['VALUE'][$key] ?></li>
							<? endfor ?>
						</ol>
					</div>
				<? endforeach ?>
			</div>
		</div>
	</div>
</div>
<? else : ?>
	<div align="center">
		<p style="color: red; font-size: 300%">SKILLS IS EMPTY </p>
	</div>
<? endif ?>