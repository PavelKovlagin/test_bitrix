    <footer class="footer">
        <div class="container">
            <div class="footer__inner">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include", 
                    "policy", 
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => "policy",
                        "PATH" => "include/footer/policy.php"
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include", 
                    "copyright", 
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => "copyright",
                        "PATH" => "include/footer/copyright.php"
                    ),
                    false
                );?>
            </div>
        </div>
    </footer>
    </div>
 
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/css/libs.min.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/css/main.css"/>
    <script defer="defer" src="<?=SITE_TEMPLATE_PATH ?>/js/map.js"></script>
    <script defer="defer">var mapLoaded = false; var mapTimer = 0; function createMap() { var ymap = document.querySelector('#map'); if (ymap.getBoundingClientRect().top < window.innerHeight * 2 && mapLoaded === false) { console.log('try'); clearTimeout(mapTimer); mapTimer = setTimeout(function() { console.log('go'); mapLoaded = true; var elem = document.createElement('script'); elem.type = 'text/javascript'; elem.src = "http://api-maps.yandex.ru/2.1/?apikey=1b577d37-f341-456a-8734-b6a76134da85&lang=ru_RU&onload=yandexMapInit"; document.getElementsByTagName('body')[0].appendChild(elem); }, 100); } else return; } document.addEventListener('scroll', createMap); window.addEventListener('scroll', createMap); </script>
</body>
</html>