<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

?>
<!DOCTYPE html>
<html>

<head>
	<title>
		<?$APPLICATION->ShowTitle()?>
	</title>

	<link rel="preload" href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'" />
	<noscript>
		<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet" type="text/css" />
	</noscript>
	<style>
		a,
		abbr,
		acronym,
		address,
		applet,
		article,
		aside,
		audio,
		b,
		big,
		blockquote,
		body,
		canvas,
		caption,
		center,
		cite,
		code,
		dd,
		del,
		details,
		dfn,
		div,
		dl,
		dt,
		em,
		embed,
		fieldset,
		figcaption,
		figure,
		footer,
		form,
		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		header,
		hgroup,
		html,
		i,
		iframe,
		img,
		ins,
		kbd,
		label,
		legend,
		li,
		mark,
		menu,
		nav,
		object,
		ol,
		output,
		p,
		pre,
		q,
		ruby,
		s,
		samp,
		section,
		small,
		span,
		strike,
		strong,
		sub,
		summary,
		sup,
		table,
		tbody,
		td,
		tfoot,
		th,
		thead,
		time,
		tr,
		tt,
		u,
		ul,
		var,
		video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			vertical-align: baseline
		}

		article,
		aside,
		details,
		figcaption,
		figure,
		footer,
		header,
		hgroup,
		menu,
		nav,
		section {
			display: block
		}

		body {
			line-height: 1
		}

		ol,
		ul {
			list-style: none
		}

		blockquote,
		q {
			quotes: none
		}

		blockquote:after,
		blockquote:before,
		q:after,
		q:before {
			content: "";
			content: none
		}

		table {
			border-collapse: collapse;
			border-spacing: 0
		}

		input::-ms-clear {
			display: none
		}

		*,
		:after,
		:before {
			outline: 0 !important;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
			min-width: 0;
			min-height: 0;
			-ms-touch-action: manipulation;
			touch-action: manipulation;
			letter-spacing: normal
		}

		body,
		html {
			width: 100%;
			font-size: 16px;
			line-height: normal;
			letter-spacing: normal;
			font-weight: 400;
			-webkit-overflow-scrolling: touch;
			-webkit-tap-highlight-color: transparent
		}

		body,
		button,
		html,
		input,
		select,
		textarea {
			font-family: Roboto Condensed, sans-serif
		}

		button,
		input,
		select,
		textarea {
			-webkit-appearance: none;
			border-radius: 0
		}

		textarea {
			overflow: auto
		}

		html {
			-webkit-text-size-adjust: 100%;
			font-size: 10px
		}

		svg {
			display: block;
			max-width: 100%;
			max-height: 100%
		}

		a {
			color: inherit;
			text-decoration: none;
			cursor: pointer
		}

		h1,
		h2,
		h3 {
			font-weight: 400
		}

		.wrapper {
			width: 100%;
			overflow: hidden;
			position: relative
		}

		.container {
			margin: 0 auto;
			width: 1530px
		}

		@media (max-width:1599px) {
			.container {
				width: 1170px
			}
		}

		@media (max-width:1279px) {
			.container {
				width: auto;
				margin: 0 30px
			}
		}

		@media (max-width:767px) {
			.container {
				margin: 0 15px
			}
		}

		body {
			font-size: 24px;
			font-weight: 300;
			line-height: 1.2;
			color: #000
		}

		@media (max-width:1599px) {
			body {
				font-size: 16px
			}
		}

		@-webkit-keyframes fadeDown {
			0% {
				opacity: 0;
				top: -200px
			}

			to {
				opacity: 1;
				top: 0
			}
		}

		@keyframes fadeDown {
			0% {
				opacity: 0;
				top: -200px
			}

			to {
				opacity: 1;
				top: 0
			}
		}

		@-webkit-keyframes fadeUp {
			0% {
				opacity: 0;
				-webkit-transform: translateY(100%);
				transform: translateY(100%)
			}

			to {
				opacity: 1;
				-webkit-transform: translateY(0);
				transform: translateY(0)
			}
		}

		@keyframes fadeUp {
			0% {
				opacity: 0;
				-webkit-transform: translateY(100%);
				transform: translateY(100%)
			}

			to {
				opacity: 1;
				-webkit-transform: translateY(0);
				transform: translateY(0)
			}
		}

		.header {
			position: relative;
			opacity: 0;
			-webkit-animation: fadeDown 1s ease .2s forwards;
			animation: fadeDown 1s ease .2s forwards;
			z-index: 1;
			top: 0
		}

		.main,
		.main~* {
			opacity: 0;
			-webkit-animation: fadeUp 1s ease .2s forwards;
			animation: fadeUp 1s ease .2s forwards
		}

		.header__inner {
			display: -ms-flexbox;
			display: -webkit-box;
			display: -webkit-flex;
			display: flex;
			-ms-flex-align: baseline;
			-webkit-box-align: baseline;
			-webkit-align-items: baseline;
			align-items: baseline;
			padding: 82px 0
		}

		@media (max-width:1279px) {
			.header__inner {
				padding: 52px 0
			}
		}

		@media (max-width:767px) {
			.header__inner {
				-ms-flex-pack: justify;
				-webkit-box-pack: justify;
				-webkit-justify-content: space-between;
				justify-content: space-between;
				padding: 30px 0;
				-ms-flex-align: center;
				-webkit-box-align: center;
				-webkit-align-items: center;
				align-items: center
			}
		}

		.logo {
			margin-right: 49px;
			display: block;
			position: relative;
			z-index: 100
		}

		@media (min-width:1280px) {
			.logo {
				top: 1px
			}
		}

		@media (max-width:1599px) {
			.logo {
				margin-right: 47px
			}
		}

		@media (max-width:1279px) {
			.logo {
				margin-right: 27px
			}
		}

		.logo img {
			display: block;
			height: 29px
		}

		@media (max-width:1599px) {
			.logo img {
				height: 20px
			}
		}

		.burger {
			display: none
		}

		@media (max-width:767px) {
			.burger {
				display: block;
				width: 25px;
				height: 14px;
				position: relative;
				cursor: pointer;
				z-index: 100
			}

			.burger:hover div:first-child {
				top: -2px;
				-webkit-transition: .2s ease-in-out;
				-o-transition: .2s ease-in-out;
				transition: .2s ease-in-out
			}

			.burger:hover div:nth-child(3) {
				top: 14px;
				-webkit-transition: .16s ease-in-out;
				-o-transition: .16s ease-in-out;
				transition: .16s ease-in-out
			}

			.burger div {
				z-index: 3;
				display: block;
				position: absolute;
				height: 2px;
				width: 100%;
				background: #303030;
				border-radius: 2px;
				opacity: 1;
				left: 0;
				-webkit-transform: rotate(0);
				-ms-transform: rotate(0);
				transform: rotate(0);
				-webkit-transition: .25s ease-in-out;
				-o-transition: .25s ease-in-out;
				transition: .25s ease-in-out
			}

			.burger div:first-child {
				top: 0
			}

			.burger div:nth-child(2) {
				top: 6px
			}

			.burger div:nth-child(3) {
				top: 12px
			}

			.burger.is-open div:first-child {
				top: 6px;
				-webkit-transform: rotate(135deg);
				-ms-transform: rotate(135deg);
				transform: rotate(135deg)
			}

			.burger.is-open div:nth-child(2) {
				opacity: 0;
				left: -30px;
				-webkit-transition: .16s ease-in-out;
				-o-transition: .16s ease-in-out;
				transition: .16s ease-in-out
			}

			.burger.is-open div:nth-child(3) {
				top: 6px;
				-webkit-transform: rotate(-135deg);
				-ms-transform: rotate(-135deg);
				transform: rotate(-135deg)
			}
		}

		.menu__inner {
			display: -ms-flexbox;
			display: -webkit-box;
			display: -webkit-flex;
			display: flex
		}

		@media (max-width:767px) {
			.menu__inner {
				display: block;
				height: 100%;
				overflow-x: hidden;
				overflow-y: auto
			}
		}

		@media (max-width:767px) {
			.menu {
				position: fixed;
				top: 80px;
				bottom: 0;
				z-index: 99;
				padding: 0 15px;
				display: block;
				visibility: hidden;
				-webkit-transform: translateX(100%);
				-ms-transform: translateX(100%);
				transform: translateX(100%);
				-webkit-transition: visibility 0s ease 1.5s, -webkit-transform 1s ease .5s;
				transition: visibility 0s ease 1.5s, -webkit-transform 1s ease .5s;
				-o-transition: transform 1s ease .5s, visibility 0s ease 1.5s;
				transition: transform 1s ease .5s, visibility 0s ease 1.5s;
				transition: transform 1s ease .5s, visibility 0s ease 1.5s, -webkit-transform 1s ease .5s;
				padding-bottom: 30px
			}
		}

		@media (max-width:767px) {

			.menu,
			.menu:before {
				left: 0;
				right: 0;
				background-color: #fff
			}

			.menu:before {
				content: "";
				position: absolute;
				bottom: 100%;
				height: 80px
			}
		}

		@media (max-width:767px) {
			.menu.is-open {
				visibility: visible;
				-webkit-transform: translateX(0);
				-ms-transform: translateX(0);
				transform: translateX(0);
				-webkit-transition: visibility 0s ease 0s, -webkit-transform 1s ease 0s;
				transition: visibility 0s ease 0s, -webkit-transform 1s ease 0s;
				-o-transition: transform 1s ease 0s, visibility 0s ease 0s;
				transition: transform 1s ease 0s, visibility 0s ease 0s;
				transition: transform 1s ease 0s, visibility 0s ease 0s, -webkit-transform 1s ease 0s
			}
		}

		.menu__item {
			margin-right: 46px
		}

		.menu__item:last-child {
			margin-right: 0
		}

		@media (max-width:1599px) {
			.menu__item {
				margin-right: 32px
			}
		}

		@media (max-width:1279px) {
			.menu__item {
				margin-right: 20px
			}
		}

		@media (max-width:767px) {
			.menu__item {
				padding: 10px 0 11px;
				border-bottom: 1px solid #bbb;
				margin: 0;
				-webkit-transform: translateX(100%);
				-ms-transform: translateX(100%);
				transform: translateX(100%)
			}

			.menu__item:first-child {
				-webkit-transition: -webkit-transform .5s ease-in-out .15s;
				transition: -webkit-transform .5s ease-in-out .15s;
				-o-transition: transform .5s ease-in-out .15s;
				transition: transform .5s ease-in-out .15s;
				transition: transform .5s ease-in-out .15s, -webkit-transform .5s ease-in-out .15s
			}

			.menu__item:nth-child(2) {
				-webkit-transition: -webkit-transform .5s ease-in-out .3s;
				transition: -webkit-transform .5s ease-in-out .3s;
				-o-transition: transform .5s ease-in-out .3s;
				transition: transform .5s ease-in-out .3s;
				transition: transform .5s ease-in-out .3s, -webkit-transform .5s ease-in-out .3s
			}

			.menu__item:nth-child(3) {
				-webkit-transition: -webkit-transform .5s ease-in-out .45s;
				transition: -webkit-transform .5s ease-in-out .45s;
				-o-transition: transform .5s ease-in-out .45s;
				transition: transform .5s ease-in-out .45s;
				transition: transform .5s ease-in-out .45s, -webkit-transform .5s ease-in-out .45s
			}

			.menu__item:nth-child(4) {
				-webkit-transition: -webkit-transform .5s ease-in-out .6s;
				transition: -webkit-transform .5s ease-in-out .6s;
				-o-transition: transform .5s ease-in-out .6s;
				transition: transform .5s ease-in-out .6s;
				transition: transform .5s ease-in-out .6s, -webkit-transform .5s ease-in-out .6s
			}

			.menu__item:nth-child(5) {
				-webkit-transition: -webkit-transform .5s ease-in-out .75s;
				transition: -webkit-transform .5s ease-in-out .75s;
				-o-transition: transform .5s ease-in-out .75s;
				transition: transform .5s ease-in-out .75s;
				transition: transform .5s ease-in-out .75s, -webkit-transform .5s ease-in-out .75s
			}

			.menu__item:nth-child(6) {
				-webkit-transition: -webkit-transform .5s ease-in-out .9s;
				transition: -webkit-transform .5s ease-in-out .9s;
				-o-transition: transform .5s ease-in-out .9s;
				transition: transform .5s ease-in-out .9s;
				transition: transform .5s ease-in-out .9s, -webkit-transform .5s ease-in-out .9s
			}

			.menu__item:nth-child(7) {
				-webkit-transition: -webkit-transform .5s ease-in-out 1.05s;
				transition: -webkit-transform .5s ease-in-out 1.05s;
				-o-transition: transform .5s ease-in-out 1.05s;
				transition: transform .5s ease-in-out 1.05s;
				transition: transform .5s ease-in-out 1.05s, -webkit-transform .5s ease-in-out 1.05s
			}

			.menu__item:nth-child(8) {
				-webkit-transition: -webkit-transform .5s ease-in-out 1.2s;
				transition: -webkit-transform .5s ease-in-out 1.2s;
				-o-transition: transform .5s ease-in-out 1.2s;
				transition: transform .5s ease-in-out 1.2s;
				transition: transform .5s ease-in-out 1.2s, -webkit-transform .5s ease-in-out 1.2s
			}

			.menu__item:nth-child(9) {
				-webkit-transition: -webkit-transform .5s ease-in-out 1.35s;
				transition: -webkit-transform .5s ease-in-out 1.35s;
				-o-transition: transform .5s ease-in-out 1.35s;
				transition: transform .5s ease-in-out 1.35s;
				transition: transform .5s ease-in-out 1.35s, -webkit-transform .5s ease-in-out 1.35s
			}

			.menu__item:nth-child(10) {
				-webkit-transition: -webkit-transform .5s ease-in-out 1.5s;
				transition: -webkit-transform .5s ease-in-out 1.5s;
				-o-transition: transform .5s ease-in-out 1.5s;
				transition: transform .5s ease-in-out 1.5s;
				transition: transform .5s ease-in-out 1.5s, -webkit-transform .5s ease-in-out 1.5s
			}
		}

		@media (max-width:767px) {
			.menu.is-open .menu__item {
				-webkit-transform: translateX(0);
				-ms-transform: translateX(0);
				transform: translateX(0)
			}
		}

		.menu__link {
			font-size: 23px;
			font-family: Oswald;
			line-height: 34px;
			font-weight: 400;
			color: #eb5757;
			position: relative;
			-webkit-transition: color .3s ease 0s;
			-o-transition: color .3s ease 0s;
			transition: color .3s ease 0s
		}

		@media (max-width:1599px) {
			.menu__link {
				font-size: 19px;
				line-height: 28px
			}
		}

		@media (max-width:1279px) {
			.menu__link {
				font-size: 16px
			}
		}

		@media (max-width:767px) {
			.menu__link {
				color: #202020;
				line-height: 24px
			}
		}

		.menu__link:after {
			content: "";
			position: absolute;
			display: block;
			width: 14px;
			height: 11px;
			background-repeat: no-repeat;
			background-size: contain;
			background-position: top;
			background-image: url(assets/images/menu-icon.svg);
			top: 100%;
			left: 0;
			right: 0;
			margin: 4px auto 0;
			opacity: 0;
			-webkit-transition: opacity .3s ease 0s;
			-o-transition: opacity .3s ease 0s;
			transition: opacity .3s ease 0s
		}

		@media (max-width:767px) {
			.menu__link:after {
				content: none
			}
		}

		@media (min-width:1280px) {
			.menu__link:hover {
				color: #202020
			}

			.menu__link:hover:after {
				opacity: 1
			}
		}

		.menu__link:active {
			color: #202020
		}

		.menu__link:active:after {
			opacity: 1
		}

		@media (max-width:767px) {
			.menu__link:active {
				color: #eb5757
			}
		}

		.main {
			padding: 27px 0 0;
			margin: 0 0 120px
		}

		@media (max-width:1599px) {
			.main {
				padding-top: 33px;
				margin-bottom: 90px
			}
		}

		@media (max-width:767px) {
			.main {
				margin-bottom: 40px;
				padding-top: 10px
			}
		}

		.main__inner {
			width: 990px;
			max-width: 100%
		}

		@media (max-width:1599px) {
			.main__inner {
				width: 740px
			}
		}

		.main__arrows {
			margin: 60px 0 78px
		}

		@media (max-width:767px) {
			.main__arrows {
				margin: 20px 0 30px;
				width: 100px
			}
		}

		.main__previews {
			display: -ms-flexbox;
			display: -webkit-box;
			display: -webkit-flex;
			display: flex
		}

		@media (max-width:1279px) {
			.main__previews {
				width: -webkit-calc(100vw - 160px);
				width: calc(100vw - 160px);
				min-width: 740px
			}
		}

		@media (max-width:767px) {
			.main__previews {
				width: 100%;
				min-width: 0
			}
		}

		.main__previews-item {
			width: 362px;
			height: 241px;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: 50%;
			-ms-flex-negative: 0;
			-webkit-flex-shrink: 0;
			flex-shrink: 0;
			margin-right: 1px
		}

		@media (max-width:1599px) {
			.main__previews-item {
				width: 292px;
				height: 192px
			}
		}

		@media (max-width:1279px) {
			.main__previews-item {
				-ms-flex: 1 0 auto;
				-webkit-box-flex: 1;
				-webkit-flex: 1 0 auto;
				flex: 1 0 auto;
				width: auto;
				height: auto
			}
		}

		@media (max-width:1279px) {
			.main__previews-item:after {
				content: "";
				display: block;
				padding-top: 65.75342%
			}
		}

		@media (max-width:767px) {
			.main__previews-item:after {
				padding-top: 66.36364%
			}
		}

		.h1 {
			display: block;
			font-weight: 600;
			font-size: 55px;
			color: #202020;
			margin-bottom: 40px;
			line-height: inherit;
			font-family: Oswald
		}

		@media (max-width:1599px) {
			.h1 {
				font-size: 40px;
				margin-bottom: 30px
			}
		}

		@media (max-width:767px) {
			.h1 {
				font-size: 25px;
				margin-bottom: 20px
			}
		}

		.h2 {
			display: block;
			font-weight: 500;
			font-size: 45px;
			color: #202020;
			margin-bottom: 40px;
			line-height: 67px;
			font-family: Oswald
		}

		@media (max-width:1599px) {
			.h2 {
				font-size: 30px;
				line-height: 44px
			}
		}

		@media (max-width:767px) {
			.h2 {
				font-size: 22px;
				line-height: 33px;
				margin-bottom: 20px
			}
		}

		.list a,
		.text a {
			text-decoration: underline;
			color: #eb5757;
			-webkit-text-decoration-skip-ink: none;
			text-decoration-skip-ink: none
		}

		.start-anim {
			z-index: -1;
			position: absolute;
			width: 0;
			height: 0;
			top: 0;
			right: 0
		}

		@media (max-width:1279px) {
			.start-anim {
				display: none
			}
		}

		.start-anim .circle {
			border-radius: 50%;
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			border: 1px solid #000;
			-webkit-transform-origin: center center;
			-ms-transform-origin: center center;
			transform-origin: center center;
			-webkit-transition: left 1s ease 0s, top 1s ease 0s, right 1s ease 0s, bottom 1s ease 0s, -webkit-transform 1s ease 1s;
			transition: left 1s ease 0s, top 1s ease 0s, right 1s ease 0s, bottom 1s ease 0s, -webkit-transform 1s ease 1s;
			-o-transition: left 1s ease 0s, top 1s ease 0s, right 1s ease 0s, bottom 1s ease 0s, transform 1s ease 1s;
			transition: left 1s ease 0s, top 1s ease 0s, right 1s ease 0s, bottom 1s ease 0s, transform 1s ease 1s;
			transition: left 1s ease 0s, top 1s ease 0s, right 1s ease 0s, bottom 1s ease 0s, transform 1s ease 1s, -webkit-transform 1s ease 1s
		}

		.start-anim .circle:nth-child(2n) {
			border-color: #eb5757
		}

		.start-anim .circle:first-child {
			-webkit-transform: rotate(-52deg);
			-ms-transform: rotate(-52deg);
			transform: rotate(-52deg);
			-webkit-transition: left 1s ease .4s, top 1s ease .4s, right 1s ease .4s, bottom 1s ease .4s, -webkit-transform 1s ease 1.4s;
			transition: left 1s ease .4s, top 1s ease .4s, right 1s ease .4s, bottom 1s ease .4s, -webkit-transform 1s ease 1.4s;
			-o-transition: left 1s ease .4s, top 1s ease .4s, right 1s ease .4s, bottom 1s ease .4s, transform 1s ease 1.4s;
			transition: left 1s ease .4s, top 1s ease .4s, right 1s ease .4s, bottom 1s ease .4s, transform 1s ease 1.4s;
			transition: left 1s ease .4s, top 1s ease .4s, right 1s ease .4s, bottom 1s ease .4s, transform 1s ease 1.4s, -webkit-transform 1s ease 1.4s
		}

		.start-anim .circle:first-child .icon {
			-webkit-transform: rotate(52deg);
			-ms-transform: rotate(52deg);
			transform: rotate(52deg);
			-webkit-transition: visibility 0s ease 1.4s, -webkit-transform 1s ease 1.4s;
			transition: visibility 0s ease 1.4s, -webkit-transform 1s ease 1.4s;
			-o-transition: visibility 0s ease 1.4s, transform 1s ease 1.4s;
			transition: visibility 0s ease 1.4s, transform 1s ease 1.4s;
			transition: visibility 0s ease 1.4s, transform 1s ease 1.4s, -webkit-transform 1s ease 1.4s
		}

		.start-anim .circle:nth-child(2) {
			-webkit-transform: rotate(35deg);
			-ms-transform: rotate(35deg);
			transform: rotate(35deg);
			-webkit-transition: left 1s ease .3s, top 1s ease .3s, right 1s ease .3s, bottom 1s ease .3s, -webkit-transform 1s ease 1.3s;
			transition: left 1s ease .3s, top 1s ease .3s, right 1s ease .3s, bottom 1s ease .3s, -webkit-transform 1s ease 1.3s;
			-o-transition: left 1s ease .3s, top 1s ease .3s, right 1s ease .3s, bottom 1s ease .3s, transform 1s ease 1.3s;
			transition: left 1s ease .3s, top 1s ease .3s, right 1s ease .3s, bottom 1s ease .3s, transform 1s ease 1.3s;
			transition: left 1s ease .3s, top 1s ease .3s, right 1s ease .3s, bottom 1s ease .3s, transform 1s ease 1.3s, -webkit-transform 1s ease 1.3s
		}

		.start-anim .circle:nth-child(2) .icon {
			-webkit-transform: rotate(-35deg);
			-ms-transform: rotate(-35deg);
			transform: rotate(-35deg);
			-webkit-transition: visibility 0s ease 1.3s, -webkit-transform 1s ease 1.3s;
			transition: visibility 0s ease 1.3s, -webkit-transform 1s ease 1.3s;
			-o-transition: visibility 0s ease 1.3s, transform 1s ease 1.3s;
			transition: visibility 0s ease 1.3s, transform 1s ease 1.3s;
			transition: visibility 0s ease 1.3s, transform 1s ease 1.3s, -webkit-transform 1s ease 1.3s
		}

		.start-anim .circle:nth-child(3) {
			-webkit-transform: rotate(-85deg);
			-ms-transform: rotate(-85deg);
			transform: rotate(-85deg);
			-webkit-transition: left 1s ease .2s, top 1s ease .2s, right 1s ease .2s, bottom 1s ease .2s, -webkit-transform 1s ease 1.2s;
			transition: left 1s ease .2s, top 1s ease .2s, right 1s ease .2s, bottom 1s ease .2s, -webkit-transform 1s ease 1.2s;
			-o-transition: left 1s ease .2s, top 1s ease .2s, right 1s ease .2s, bottom 1s ease .2s, transform 1s ease 1.2s;
			transition: left 1s ease .2s, top 1s ease .2s, right 1s ease .2s, bottom 1s ease .2s, transform 1s ease 1.2s;
			transition: left 1s ease .2s, top 1s ease .2s, right 1s ease .2s, bottom 1s ease .2s, transform 1s ease 1.2s, -webkit-transform 1s ease 1.2s
		}

		.start-anim .circle:nth-child(3) .icon {
			-webkit-transform: rotate(85deg);
			-ms-transform: rotate(85deg);
			transform: rotate(85deg);
			-webkit-transition: visibility 0s ease 1.2s, -webkit-transform 1s ease 1.2s;
			transition: visibility 0s ease 1.2s, -webkit-transform 1s ease 1.2s;
			-o-transition: visibility 0s ease 1.2s, transform 1s ease 1.2s;
			transition: visibility 0s ease 1.2s, transform 1s ease 1.2s;
			transition: visibility 0s ease 1.2s, transform 1s ease 1.2s, -webkit-transform 1s ease 1.2s
		}

		.start-anim .circle:nth-child(4) {
			-webkit-transition: left 1s ease .1s, top 1s ease .1s, right 1s ease .1s, bottom 1s ease .1s, -webkit-transform 1s ease 1.1s;
			transition: left 1s ease .1s, top 1s ease .1s, right 1s ease .1s, bottom 1s ease .1s, -webkit-transform 1s ease 1.1s;
			-o-transition: left 1s ease .1s, top 1s ease .1s, right 1s ease .1s, bottom 1s ease .1s, transform 1s ease 1.1s;
			transition: left 1s ease .1s, top 1s ease .1s, right 1s ease .1s, bottom 1s ease .1s, transform 1s ease 1.1s;
			transition: left 1s ease .1s, top 1s ease .1s, right 1s ease .1s, bottom 1s ease .1s, transform 1s ease 1.1s, -webkit-transform 1s ease 1.1s
		}

		.start-anim .circle_full {
			background-color: #eb5757;
			border: none
		}

		.start-anim .circle .icon {
			position: absolute;
			-webkit-transform-origin: center center;
			-ms-transform-origin: center center;
			transform-origin: center center;
			visibility: hidden
		}

		.start-anim .circle:first-child .icon {
			width: 100px;
			height: 100px;
			left: 152px;
			bottom: 189px
		}

		@media (max-width:1599px) {
			.start-anim .circle:first-child .icon {
				width: 68px;
				height: 68px;
				left: 102px;
				bottom: 127px
			}
		}

		.start-anim .circle:nth-child(2) .icon {
			width: 85px;
			height: 85px;
			left: 21px;
			bottom: 287px
		}

		@media (max-width:1599px) {
			.start-anim .circle:nth-child(2) .icon {
				width: 57px;
				height: 57px;
				left: 14px;
				bottom: 193px
			}
		}

		.start-anim .circle:nth-child(3) .icon {
			width: 78px;
			height: 78px;
			left: -27px;
			bottom: 312px
		}

		@media (max-width:1599px) {
			.start-anim .circle:nth-child(3) .icon {
				height: 53px;
				width: 53px;
				bottom: 210px;
				left: -18px
			}
		}

		.start-anim .circle.is-show {
			-webkit-transform: rotate(0);
			-ms-transform: rotate(0);
			transform: rotate(0)
		}

		.start-anim .circle.is-show .icon {
			visibility: visible;
			-webkit-transform: rotate(0);
			-ms-transform: rotate(0);
			transform: rotate(0)
		}

		.start-anim .circle.is-show:first-child {
			top: -750px;
			left: -750px;
			bottom: -750px;
			right: -750px
		}

		@media (max-width:1599px) {
			.start-anim .circle.is-show:first-child {
				top: -505px;
				left: -505px;
				bottom: -505px;
				right: -505px
			}
		}

		.start-anim .circle.is-show:nth-child(2) {
			left: -600px;
			top: -600px;
			right: -600px;
			bottom: -600px
		}

		@media (max-width:1599px) {
			.start-anim .circle.is-show:nth-child(2) {
				left: -404px;
				top: -404px;
				right: -404px;
				bottom: -404px
			}
		}

		.start-anim .circle.is-show:nth-child(3) {
			left: -450px;
			top: -450px;
			right: -450px;
			bottom: -450px
		}

		@media (max-width:1599px) {
			.start-anim .circle.is-show:nth-child(3) {
				left: -303px;
				top: -303px;
				right: -303px;
				bottom: -303px
			}
		}

		.start-anim .circle.is-show:nth-child(4) {
			left: -300px;
			top: -300px;
			right: -300px;
			bottom: -300px
		}

		@media (max-width:1599px) {
			.start-anim .circle.is-show:nth-child(4) {
				left: -202px;
				top: -202px;
				right: -202px;
				bottom: -202px
			}
		}

		.start-anim .circle.is-show:nth-child(5) {
			left: -150px;
			top: -150px;
			right: -150px;
			bottom: -150px
		}

		@media (max-width:1599px) {
			.start-anim .circle.is-show:nth-child(5) {
				left: -101px;
				top: -101px;
				right: -101px;
				bottom: -101px
			}
		}

		.start-anim .star-1 {
			width: 60px;
			height: 60px;
			position: absolute;
			top: 178px;
			right: 767px;
			-webkit-transform: scale(0) translateZ(0);
			transform: scale(0) translateZ(0);
			-webkit-transform-origin: center center;
			-ms-transform-origin: center center;
			transform-origin: center center;
			-webkit-transition: -webkit-transform 1s linear .1s;
			transition: -webkit-transform 1s linear .1s;
			-o-transition: transform 1s linear .1s;
			transition: transform 1s linear .1s;
			transition: transform 1s linear .1s, -webkit-transform 1s linear .1s
		}

		@media (max-width:1599px) {
			.start-anim .star-1 {
				width: 38px;
				height: 38px;
				top: 122px;
				right: 519px
			}
		}

		.start-anim .star-1.is-show {
			-webkit-transform: scale(1) translateZ(0);
			transform: scale(1) translateZ(0)
		}

		.start-anim .star-1 svg {
			display: block;
			width: 100%;
			height: 100%
		}

		.start-anim .star-2 {
			width: 32px;
			height: 32px;
			position: absolute;
			top: 326px;
			right: 134px;
			-webkit-transform: scale(0) translateZ(0);
			transform: scale(0) translateZ(0);
			-webkit-transform-origin: center center;
			-ms-transform-origin: center center;
			transform-origin: center center;
			-webkit-transition: -webkit-transform 1s linear .4s;
			transition: -webkit-transform 1s linear .4s;
			-o-transition: transform 1s linear .4s;
			transition: transform 1s linear .4s;
			transition: transform 1s linear .4s, -webkit-transform 1s linear .4s
		}

		@media (max-width:1599px) {
			.start-anim .star-2 {
				width: 20px;
				height: 20px;
				right: 92px;
				top: 221px
			}
		}

		.start-anim .star-2.is-show {
			-webkit-transform: scale(1) translateZ(0);
			transform: scale(1) translateZ(0)
		}

		.start-anim .star-2 svg {
			display: block;
			width: 100%;
			height: 100%
		}

		.start-anim .star-3 {
			width: 200px;
			height: 200px;
			position: absolute;
			top: 703px;
			right: 294px;
			-webkit-transform: scale(0) translateZ(0);
			transform: scale(0) translateZ(0);
			-webkit-transform-origin: center center;
			-ms-transform-origin: center center;
			transform-origin: center center;
			-webkit-transition: -webkit-transform 1s linear .7s;
			transition: -webkit-transform 1s linear .7s;
			-o-transition: transform 1s linear .7s;
			transition: transform 1s linear .7s;
			transition: transform 1s linear .7s, -webkit-transform 1s linear .7s
		}

		@media (max-width:1599px) {
			.start-anim .star-3 {
				width: 134px;
				height: 134px;
				right: 132px;
				top: 514px
			}
		}

		.start-anim .star-3.is-show {
			-webkit-transform: scale(1) translateZ(0);
			transform: scale(1) translateZ(0)
		}

		.start-anim .star-3 svg {
			display: block;
			width: 100%;
			height: 100%
		}
	</style>
	<script defer="defer" src="<?= SITE_TEMPLATE_PATH ?>/js/libs.min.js"></script>
	<script defer="defer" src="<?= SITE_TEMPLATE_PATH ?>/js/common.js"></script>

	<link rel="shortcut icon" type="image/x-icon" href="<?SITE_DIR?>/favicon.ico">

	<?$APPLICATION->ShowHead();?>
</head>

<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>

	<div class="wrapper">

		<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"menu", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_HANDLERS" => array(
			0 => "delicious",
		),
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "Hawking_school",
		"IBLOCK_ID" => "20",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => "menu",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DESCRIPTION",
			2 => "",
		),
		"IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
		"DETAIL_URL" => "",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"GROUP_PERMISSIONS" => array(
			0 => "1",
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"STRICT_SECTION_CHECK" => "Y",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "menu",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILE_404" => ""
	),
	false
);?>

		<div class="start-anim">
			<div class="circle js-circle">
				<div class="icon"><svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="50" cy="50" r="49.5" fill="white" stroke="black" />
						<path d="M38 40L28 50L38 60M60 40L70 50L60 60M54 29L44 70.3007" stroke="black" />
						<circle cx="50" cy="50" r="38.5" stroke="black" stroke-dasharray="5 5" /></svg></div>
			</div>
			<div class="circle js-circle">
				<div class="icon"><svg width="85" height="85" viewBox="0 0 85 85" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="42.5" cy="42.5" r="42" fill="white" stroke="#EB5757" />
						<path d="M43.248 27L12 37.1187L43.248 48.0042L74 37.1187L43.248 27Z" stroke="#EB5757" />
						<path d="M20.031 40V49" stroke="#EB5757" />
						<path d="M28.5 51.358V40.3886C38.3468 37.9134 48.6532 37.9134 58.5 40.3886V51.358C48.6483 48.9236 38.3517 48.9236 28.5 51.358Z" fill="white" stroke="#EB5757" />
						<path d="M58.5 52C58.5 52.0191 58.4921 52.0989 58.3415 52.2414C58.1909 52.3838 57.9412 52.545 57.5739 52.7131C56.8422 53.0479 55.7518 53.362 54.3651 53.6304C51.6 54.1656 47.7583 54.5 43.5 54.5C39.2417 54.5 35.4 54.1656 32.6349 53.6304C31.2482 53.362 30.1578 53.0479 29.4261 52.7131C29.0588 52.545 28.8091 52.3838 28.6585 52.2414C28.5079 52.0989 28.5 52.0191 28.5 52C28.5 51.9809 28.5079 51.9011 28.6585 51.7586C28.8091 51.6162 29.0588 51.455 29.4261 51.2869C30.1578 50.9521 31.2482 50.638 32.6349 50.3696C35.4 49.8344 39.2417 49.5 43.5 49.5C47.7583 49.5 51.6 49.8344 54.3651 50.3696C55.7518 50.638 56.8422 50.9521 57.5739 51.2869C57.9412 51.455 58.1909 51.6162 58.3415 51.7586C58.4921 51.9011 58.5 51.9809 58.5 52Z" stroke="#EB5757" />
						<path d="M20.5075 54.3621C20.3594 54.8262 19.7028 54.8261 19.5548 54.3621L17.8925 49.152C17.7895 48.8294 18.0302 48.5 18.3688 48.5H21.6935C22.0321 48.5 22.2728 48.8294 22.1699 49.152L20.5075 54.3621Z" stroke="#EB5757" /></svg></div>
			</div>
			<div class="circle js-circle">
				<div class="icon"><svg width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="39" cy="39" r="29.5" fill="white" stroke="black" />
						<path d="M29 39C29 44.5228 33.4772 49 39 49C44.5228 49 49 44.5228 49 39C49 33.4772 44.5228 29 39 29C33.4772 29 29 33.4772 29 39ZM29 39H23M48.5 39H54.5M38.75 48.75V54.75M38.75 23.25V29.25M31.8557 45.8943L27.6131 50.1369M49.8869 27.8631L45.6443 32.1057M31.8557 32.1057L27.6131 27.8631M49.887 50.1369L45.6443 45.8943M46 39C46 42.866 42.866 46 39 46C35.134 46 32 42.866 32 39C32 35.134 35.134 32 39 32C42.866 32 46 35.134 46 39Z" stroke="black" />
						<circle cx="39" cy="39" r="38.5" stroke="black" stroke-dasharray="5 5" /></svg></div>
			</div>
			<div class="circle js-circle"></div>
			<div class="circle js-circle circle_full"></div>
			<div class="star-1 star js-star js-rotate-scroll" data-direction="rtl"><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60" fill="none">
					<path d="M24.8418 10.3805C26.027 7.17768 26.8981 4.82685 27.7289 3.2707C28.575 1.68584 29.266 1.14463 30 1.14463C30.734 1.14463 31.425 1.68584 32.2711 3.2707C33.1019 4.82685 33.973 7.17768 35.1582 10.3806L38.0268 18.1329C38.0408 18.1707 38.0546 18.208 38.0682 18.2449C38.4989 19.4098 38.7705 20.1442 39.3131 20.6869C39.8558 21.2295 40.5902 21.5011 41.7551 21.9318C41.792 21.9454 41.8293 21.9592 41.8671 21.9732L49.6195 24.8418C52.8223 26.027 55.1731 26.8981 56.7293 27.7289C58.3142 28.575 58.8554 29.266 58.8554 30C58.8554 30.734 58.3142 31.425 56.7293 32.2711C55.1731 33.1019 52.8223 33.973 49.6194 35.1582L41.8671 38.0268C41.8293 38.0408 41.792 38.0546 41.7551 38.0682C40.5902 38.4989 39.8558 38.7705 39.3131 39.3131C38.7705 39.8558 38.4989 40.5902 38.0682 41.7551C38.0546 41.792 38.0408 41.8293 38.0268 41.8671L35.1582 49.6195C33.973 52.8223 33.1019 55.1731 32.2711 56.7293C31.425 58.3142 30.734 58.8554 30 58.8554C29.266 58.8554 28.575 58.3142 27.7289 56.7293C26.8981 55.1731 26.027 52.8223 24.8418 49.6194L21.9732 41.8671C21.9592 41.8293 21.9454 41.792 21.9318 41.7551C21.5011 40.5902 21.2295 39.8558 20.6869 39.3131C20.1442 38.7705 19.4098 38.4989 18.2449 38.0682C18.208 38.0546 18.1707 38.0408 18.1329 38.0268L10.3805 35.1582C7.17768 33.973 4.82685 33.1019 3.2707 32.2711C1.68584 31.425 1.14463 30.734 1.14463 30C1.14463 29.266 1.68584 28.575 3.2707 27.7289C4.82685 26.8981 7.17768 26.027 10.3806 24.8418L18.1329 21.9732C18.1707 21.9592 18.208 21.9454 18.2449 21.9318C19.4098 21.5011 20.1442 21.2295 20.6869 20.6869C21.2295 20.1442 21.5011 19.4098 21.9318 18.2449C21.9454 18.208 21.9592 18.1707 21.9732 18.1329L24.8418 10.3805Z" stroke="black" /></svg></div>
			<div class="star-2 star js-star js-rotate-scroll" data-direction="rtl"><svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" fill="none">
					<path d="M13.7175 7.31155C14.5095 5.17117 15.0858 3.6172 15.6329 2.59229C16.1954 1.53868 16.6141 1.26308 17 1.26308C17.3859 1.26308 17.8046 1.53868 18.3671 2.59229C18.9142 3.6172 19.4905 5.17116 20.2825 7.31154L21.2946 10.0467C21.307 10.0802 21.3192 10.1132 21.3312 10.1458C21.6091 10.8981 21.8024 11.4212 22.1906 11.8094C22.5788 12.1976 23.1019 12.3909 23.8542 12.6688C23.8868 12.6808 23.9198 12.693 23.9533 12.7054L26.6885 13.7175C28.8288 14.5095 30.3828 15.0858 31.4077 15.6329C32.4613 16.1954 32.7369 16.6141 32.7369 17C32.7369 17.3859 32.4613 17.8046 31.4077 18.3671C30.3828 18.9142 28.8288 19.4905 26.6885 20.2825L23.9533 21.2946C23.9198 21.307 23.8868 21.3192 23.8542 21.3312C23.1019 21.6091 22.5788 21.8024 22.1906 22.1906C21.8024 22.5788 21.6091 23.1019 21.3312 23.8542C21.3192 23.8868 21.307 23.9198 21.2946 23.9533L20.2825 26.6885C19.4905 28.8288 18.9142 30.3828 18.3671 31.4077C17.8046 32.4613 17.3859 32.7369 17 32.7369C16.6141 32.7369 16.1954 32.4613 15.6329 31.4077C15.0858 30.3828 14.5095 28.8288 13.7175 26.6885L12.7054 23.9533C12.693 23.9198 12.6808 23.8868 12.6688 23.8542C12.3909 23.1019 12.1976 22.5788 11.8094 22.1906C11.4212 21.8024 10.8981 21.6091 10.1458 21.3312C10.1132 21.3192 10.0802 21.307 10.0467 21.2946L7.31155 20.2825C5.17117 19.4905 3.6172 18.9142 2.59229 18.3671C1.53868 17.8046 1.26308 17.3859 1.26308 17C1.26308 16.6141 1.53868 16.1954 2.59229 15.6329C3.6172 15.0858 5.17116 14.5095 7.31154 13.7175L10.0467 12.7054C10.0802 12.693 10.1132 12.6808 10.1458 12.6688C10.8981 12.3909 11.4212 12.1976 11.8094 11.8094C12.1976 11.4212 12.3909 10.8981 12.6688 10.1458C12.6808 10.1132 12.693 10.0802 12.7054 10.0467L13.7175 7.31155Z" stroke="black" /></svg></div>
			<div class="star-3 star js-star js-rotate-scroll" data-direction="ltl"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200" fill="none">
					<path d="M188.245 100C140.795 104.729 103.063 142.462 98.3333 189.912C93.6041 142.462 55.8712 104.729 8.42119 100C55.8712 95.2708 93.6041 57.5379 98.3333 10.088C103.063 57.5379 140.795 95.2708 188.245 100Z" stroke="black" /></svg></div>
		</div>