"use strict";

function _sendFeedback() {
  var form = BX.ajax.prepareForm(document.forms.feedback).data;

  BX.ajax({   
      url: 'ajax.php',
      data: {
        form
      },
      method: 'POST',
      dataType: 'json',
      onsuccess: function(data){
          
      },
      onfailure: function(){
        
      }
  });
}

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e3) { throw _e3; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e4) { didErr = true; err = _e4; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

document.addEventListener("DOMContentLoaded", function () {
  function e() {
    var _iterator = _createForOfIteratorHelper(document.querySelectorAll("[data-src]")),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var _e = _step.value;
        _e.getBoundingClientRect().top < 2 * window.innerHeight && !_e.classList.contains("is-loaded") && (_e.classList.add("is-loaded"), "img" === _e.tagName.toLowerCase() ? _e.src = _e.dataset.src : _e.style.backgroundImage = "url(" + _e.dataset.src + ")");
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }

  function t(e) {
    var t = document.querySelector(e) || document.querySelector(".wrapper"),
        r = window.innerWidth < 768 ? 20 : 50;
    !function (e, t, r, s, n, i, l) {
      if (e) {
        clearInterval(o);
        var a = new Date().getTime();
        o = setInterval(function () {
          var r = Math.min(1, (new Date().getTime() - a) / 1e3);
          e[t] = s + r * (n - s) + "", 1 === r && clearInterval(o);
        }, 10), e[t] = s + "";
      }
    }(document.scrollingElement || document.documentElement, "scrollTop", 0, document.documentElement.scrollTop, t.offsetTop - r);
  }

  function r() {
    var e = window.pageYOffset;

    var _iterator2 = _createForOfIteratorHelper(document.querySelectorAll(".js-directions-line")),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var t = _step2.value;
        if (t.style.display = "inline-block", t.scrollWidth < window.innerWidth) for (var r = t.children[0].offsetWidth, s = 10 * Math.ceil(window.innerWidth / r) - t.children.length, n = 0; n < s; n++) {
          t.append(t.children[0].cloneNode(!0));
        }
        t.style.display = "";
        var i = .75 * e;
        "rtl" === t.dataset.direction ? (t.style.left = -i + "px", t.children[2].getBoundingClientRect().right < 0 && t.append(t.children[2])) : (t.style.left = i + "px", t.children[t.children.length - 2].getBoundingClientRect().left > window.innerWidth && t.prepend(t.children[t.children.length - 2]));
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }

  function s() {
    var e = window.pageYOffset / 720 * 100 * 1;

    var _iterator3 = _createForOfIteratorHelper(document.querySelectorAll(".js-rotate-scroll")),
        _step3;

    try {
      for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
        var t = _step3.value;
        var r = 150 / t.offsetWidth,
            s = t.children[0],
            n = e * r;
        "rtl" === t.dataset.direction && (n *= -1), s.style.transform = "rotate(" + n + "deg)", s.style.mozTransform = "rotate(" + n + "deg)", s.style.msTransform = "rotate(" + n + "deg)", s.style.webkitTransform = "rotate(" + n + "deg)";
      }
    } catch (err) {
      _iterator3.e(err);
    } finally {
      _iterator3.f();
    }
  }

  function n() {
    var _iterator4 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-arrows")),
        _step4;

    try {
      for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
        var e = _step4.value;
        var t = (e.getBoundingClientRect().top + e.getBoundingClientRect().height / 2 - .7 * window.innerHeight) / window.innerHeight * 100;
        t > 100 && (t = 100), t < 0 && (t = 0);
        var r = "left" === e.dataset.direction ? -1 : 1;
        e.style.transform = "translateX(" + t * r + "%)", e.style.mozTransform = "translateX(" + t * r + "%)", e.style.msTransform = "translateX(" + t * r + "%)", e.style.webkitTransform = "translateX(" + t * r + "%)";
      }
    } catch (err) {
      _iterator4.e(err);
    } finally {
      _iterator4.f();
    }
  }

  e(), setTimeout(e, 500), document.addEventListener("scroll", e), window.addEventListener("scroll", e), setTimeout(function () {
    var _iterator5 = _createForOfIteratorHelper(document.querySelectorAll(".js-circle")),
        _step5;

    try {
      for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
        var e = _step5.value;
        e.classList.add("is-show");
      }
    } catch (err) {
      _iterator5.e(err);
    } finally {
      _iterator5.f();
    }

    setTimeout(function () {
      var _iterator6 = _createForOfIteratorHelper(document.querySelectorAll(".js-star")),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var e = _step6.value;
          e.classList.add("is-show");
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }
    }, 2e3);
  }, 500);
  var i = 0;
  document.querySelector(".js-burger").addEventListener("click", function () {
    clearTimeout(i), this.classList.contains("is-open") ? (document.documentElement.style.overflow = "", document.body.style.overflow = "", this.classList.remove("is-open"), document.querySelector(".js-menu").classList.remove("is-open")) : (i = setTimeout(function () {
      document.documentElement.style.overflow = "hidden", document.body.style.overflow = "hidden";
    }, 1500), this.classList.add("is-open"), document.querySelector(".js-menu").classList.add("is-open"));
  });
  var o = 0;

  var _iterator7 = _createForOfIteratorHelper(document.querySelectorAll(".js-anchor")),
      _step7;

  try {
    for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
      var l = _step7.value;
      l.addEventListener("click", function () {
        var e = this.getAttribute("href");
        "#" === e && (e = !1), t(e);
        var r = document.querySelector(".js-burger.is-open");
        return r && r.click(), !1;
      });
    }
  } catch (err) {
    _iterator7.e(err);
  } finally {
    _iterator7.f();
  }

  var _iterator8 = _createForOfIteratorHelper(document.querySelectorAll(".directions__carousel")),
      _step8;

  try {
    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
      var _e2 = _step8.value;
      new Swiper(_e2, {
        loop: !1,
        pagination: {
          el: ".directions__carousel-bullets",
          clickable: !0
        },
        navigation: {
          nextEl: ".directions__carousel-arrow_next",
          prevEl: ".directions__carousel-arrow_prev"
        },
        spaceBetween: 30,
        slidesPerView: 1,
        breakpoints: {
          1280: {
            slidesPerView: 2
          },
          768: {
            slidesPerView: 1.5
          }
        }
      });
    }
  } catch (err) {
    _iterator8.e(err);
  } finally {
    _iterator8.f();
  }

  var _iterator9 = _createForOfIteratorHelper((r(), document.addEventListener("scroll", r), window.addEventListener("resize", r), s(), document.addEventListener("scroll", s), window.addEventListener("resize", s), document.querySelectorAll(".awaits__carousel"))),
      _step9;

  try {
    for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
      var a = _step9.value;
      new Swiper(a, {
        loop: !1,
        pagination: {
          el: ".awaits__carousel-bullets",
          clickable: !0
        },
        spaceBetween: 0,
        slidesPerView: 1,
        breakpoints: {
          768: {
            slidesPerView: 3,
            allowTouchMove: !1,
            longSwipes: !1,
            shortSwipes: !1,
            simulateTouch: !1,
            followFinger: !1
          }
        }
      });
    }
  } catch (err) {
    _iterator9.e(err);
  } finally {
    _iterator9.f();
  }

  var _iterator10 = _createForOfIteratorHelper(document.querySelectorAll(".join__carousel")),
      _step10;

  try {
    for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
      var c = _step10.value;
      new Swiper(c, {
        loop: !1,
        pagination: {
          el: ".join__carousel-bullets",
          clickable: !0
        },
        navigation: {
          nextEl: ".join__carousel-arrow_next",
          prevEl: ".join__carousel-arrow_prev"
        },
        spaceBetween: 30,
        slidesPerView: 1,
        breakpoints: {
          1280: {
            slidesPerView: 1
          },
          768: {
            slidesPerView: 1.1
          }
        }
      });
    }
  } catch (err) {
    _iterator10.e(err);
  } finally {
    _iterator10.f();
  }

  var _iterator11 = _createForOfIteratorHelper(document.querySelectorAll(".js-feedback-thumb")),
      _step11;

  try {
    for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
      var d = _step11.value;
      d.addEventListener("click", function () {
        var e = this.dataset.id,
            t = this.parentElement.parentElement.parentElement;

        var _iterator22 = _createForOfIteratorHelper(t.querySelectorAll(".js-feedback-photo, .js-feedback-thumb, .js-feedback-content")),
            _step22;

        try {
          for (_iterator22.s(); !(_step22 = _iterator22.n()).done;) {
            var r = _step22.value;
            r.dataset.id !== e ? r.classList.remove("is-active") : r.classList.add("is-active");
          }
        } catch (err) {
          _iterator22.e(err);
        } finally {
          _iterator22.f();
        }

        var s = t.querySelectorAll(".js-feedback-thumb");
        s[0].dataset.id === e ? (t.querySelector(".js-feedback-arrow-prev").classList.add("is-disabled"), t.querySelector(".js-feedback-arrow-next").classList.remove("is-disabled")) : s[s.length - 1].dataset.id === e ? (t.querySelector(".js-feedback-arrow-prev").classList.remove("is-disabled"), t.querySelector(".js-feedback-arrow-next").classList.add("is-disabled")) : (t.querySelector(".js-feedback-arrow-prev").classList.remove("is-disabled"), t.querySelector(".js-feedback-arrow-next").classList.remove("is-disabled"));
      });
    }
  } catch (err) {
    _iterator11.e(err);
  } finally {
    _iterator11.f();
  }

  var _iterator12 = _createForOfIteratorHelper(document.querySelectorAll(".js-feedback-arrow-next")),
      _step12;

  try {
    for (_iterator12.s(); !(_step12 = _iterator12.n()).done;) {
      var u = _step12.value;
      u.addEventListener("click", function () {
        var e = this.parentElement.parentElement.parentElement.querySelector(".js-feedback-thumb.is-active").nextElementSibling;
        e && e.click();
      });
    }
  } catch (err) {
    _iterator12.e(err);
  } finally {
    _iterator12.f();
  }

  var _iterator13 = _createForOfIteratorHelper(document.querySelectorAll(".js-feedback-arrow-prev")),
      _step13;

  try {
    for (_iterator13.s(); !(_step13 = _iterator13.n()).done;) {
      var m = _step13.value;
      m.addEventListener("click", function () {
        var e = this.parentElement.parentElement.parentElement.querySelector(".js-feedback-thumb.is-active").previousElementSibling;
        e && e.click();
      });
    }
  } catch (err) {
    _iterator13.e(err);
  } finally {
    _iterator13.f();
  }

  var _iterator14 = _createForOfIteratorHelper((n(), document.addEventListener("scroll", n), window.addEventListener("resize", n), document.querySelectorAll(".js-input-name"))),
      _step14;

  try {
    for (_iterator14.s(); !(_step14 = _iterator14.n()).done;) {
      var f = _step14.value;
      f.addEventListener("input", function () {
        var e = this.value;
        e = e.replace(/[^a-zA-Zа-яА-Я\s\-]/gi, ""), this.value = e;
      });
    }
  } catch (err) {
    _iterator14.e(err);
  } finally {
    _iterator14.f();
  }

  var _iterator15 = _createForOfIteratorHelper(document.querySelectorAll(".js-input-number")),
      _step15;

  try {
    for (_iterator15.s(); !(_step15 = _iterator15.n()).done;) {
      var f = _step15.value;
      f.addEventListener("input", function () {
        var e = this.value;
        e = e.replace(/[^0-9]/gi, ""), this.value = e;
      });
    }
  } catch (err) {
    _iterator15.e(err);
  } finally {
    _iterator15.f();
  }

  var v = new Inputmask("+7 (999) 999-99-99");

  var _iterator16 = _createForOfIteratorHelper(document.querySelectorAll(".js-input-phone")),
      _step16;

  try {
    for (_iterator16.s(); !(_step16 = _iterator16.n()).done;) {
      var p = _step16.value;
      v.mask(p);
    }
  } catch (err) {
    _iterator16.e(err);
  } finally {
    _iterator16.f();
  }

  var _iterator17 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-button-next")),
      _step17;

  try {
    for (_iterator17.s(); !(_step17 = _iterator17.n()).done;) {
      var h = _step17.value;
      h.addEventListener("click", function () {
        var e = this.parentElement.parentElement,
            t = e.querySelector(".js-form-fields").children,
            r = !0;

        var _iterator23 = _createForOfIteratorHelper(t),
            _step23;

        try {
          for (_iterator23.s(); !(_step23 = _iterator23.n()).done;) {
            var s = _step23.value;
            s.required && 0 === s.value.length || "email" === s.type && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(s.value) || s.classList.contains("js-input-phone") && 10 !== s.inputmask.unmaskedvalue().length ? (s.classList.add("is-error"), s.classList.add("is-detect"), r = !1) : s.classList.remove("is-error");
          }
        } catch (err) {
          _iterator23.e(err);
        } finally {
          _iterator23.f();
        }

        return r && (e.classList.remove("is-active"), e.nextElementSibling.classList.add("is-active")), !1;
      });
    }
  } catch (err) {
    _iterator17.e(err);
  } finally {
    _iterator17.f();
  }

  var _iterator18 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-button-prev")),
      _step18;

  try {
    for (_iterator18.s(); !(_step18 = _iterator18.n()).done;) {
      var w = _step18.value;
      w.addEventListener("click", function () {
        var e = this.parentElement.parentElement;
        return e.classList.remove("is-active"), e.previousElementSibling.classList.add("is-active"), !1;
      });
    }
  } catch (err) {
    _iterator18.e(err);
  } finally {
    _iterator18.f();
  }

  var _iterator19 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-fields input, .js-form-fields textarea")),
      _step19;

  try {
    for (_iterator19.s(); !(_step19 = _iterator19.n()).done;) {
      var y = _step19.value;
      y.addEventListener("input", function () {
        this.classList.contains("is-detect") && (this.required && 0 === this.value.length || "email" === this.type && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.value) || this.classList.contains("js-input-phone") && 10 !== this.inputmask.unmaskedvalue().length ? this.classList.add("is-error") : this.classList.remove("is-error"));
      });
    }
  } catch (err) {
    _iterator19.e(err);
  } finally {
    _iterator19.f();
  }

  var _iterator20 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-button-submit")),
      _step20;

  try {
    for (_iterator20.s(); !(_step20 = _iterator20.n()).done;) {
      var L = _step20.value;
      L.addEventListener("click", function () {
        
        var e = this.parentElement.parentElement,
            t = e.querySelector(".js-form-fields").children,
            r = !0;

        var _iterator24 = _createForOfIteratorHelper(t),
            _step24;

        try {
          for (_iterator24.s(); !(_step24 = _iterator24.n()).done;) {
            var s = _step24.value;
            s.required && 0 === s.value.length || "email" === s.type && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(s.value) || s.classList.contains("js-input-phone") && 10 !== s.inputmask.unmaskedvalue().length ? (s.classList.add("is-error"), s.classList.add("is-detect"), r = !1) : s.classList.remove("is-error");
          }
          
          _sendFeedback();

        } catch (err) {
          _iterator24.e(err);
        } finally {
          _iterator24.f();
        }

        r && e.parentElement, r && (e.classList.remove("is-active"), e.nextElementSibling.classList.add("is-active"));
      });
    }
    
  } catch (err) {
    _iterator20.e(err);
  } finally {
    _iterator20.f();
  }

  var _iterator21 = _createForOfIteratorHelper(document.querySelectorAll(".js-form-button-reset")),
      _step21;

      

  try {
    for (_iterator21.s(); !(_step21 = _iterator21.n()).done;) {
      var b = _step21.value;
      b.addEventListener("click", function () {
        var e = this.parentElement.parentElement,
            t = e.parentElement;
        t.reset(), e.classList.remove("is-active"), t.querySelector(".js-form-step-wrap").classList.add("is-active");
      });
    }    
    
  } catch (err) {
    _iterator21.e(err);
  } finally {
    _iterator21.f();
  }
});