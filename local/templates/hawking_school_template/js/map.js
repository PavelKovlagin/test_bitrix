"use strict";

function yandexMapInit() {
  var t = document.querySelector(".map");

  if (null !== t) {
    var o = t.dataset.center.split(","),
        e = t.dataset.pin.split(","),
        n = t.dataset.text,
        a = new ymaps.Map(t, {
      center: o,
      zoom: 16,
      controls: ["zoomControl"]
    });
    a.geoObjects.add(new ymaps.Placemark(e, {
      balloonContent: "Совсем уже кукуха поехала у программистов этих ",
      iconCaption: n
    }, {
      preset: "islands#redDotIcon"
    })), ymapsTouchScroll(a);
  }
}