<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/css/reset.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/css/fonts.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/css/main.css">

	<title><?$APPLICATION->ShowTitle()?></title>

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/libs/slick.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH ?>/libs/select2/select2.min.css">

	<link rel="shortcut icon" type="image/x-icon" href="<?SITE_DIR?>/favicon.ico">

	<?$APPLICATION->ShowHead();?>
</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
